---
title: Datenschutz
footerPage: true
headerImageAlt: Datenschutzerklärung
pageColor: grey
headerImage:
    user/pages/11.datenschutz/Impressum_Header_1920x560px.jpg:
        name: Impressum_Header_1920x560px.jpg
        type: image/jpeg
        size: 1457994
        path: user/pages/11.datenschutz/Impressum_Header_1920x560px.jpg
---

# Datenschutzerklärung
Sehr geehrte Nutzer/innen,
der Schutz Ihrer Privatsphäre hat für uns einen hohen Stellenwert. Im Folgenden informieren wir Sie über die Verarbeitung Ihrer personenbezogenen Daten bei Nutzung unserer Website.

#### § 1 Information über die Erhebung personenbezogener Daten
(1) Im Folgenden informieren wir über die Erhebung personenbezogener Daten bei Nutzung unserer Website. Personenbezogene Daten sind alle Daten, die auf Sie persönlich beziehbar sind, z. B. Name, Adresse, E-Mail-Adressen, Nutzerverhalten.
(2) Verantwortlicher gem. Art. 4 Abs. 7 EU-Datenschutz-Grundverordnung (DS-GVO) ist
Op de Wisch e.V.,
vertreten durch Ute Peters, Oberstraße 14b 20144 Hamburg Telefon: 040-600 88 34 00 Telefax: 040-600 88 34 22 E-Mail: info@op-de-wisch.de (siehe unser Impressum).
Unseren Datenschutzbeauftragten erreichen Sie unter
Andreas Frick Diestelbarg 37 21039 Börnsen
Telefon: 040 - 69 70 26 50 E-Mail: info@datenschutz-frick.de
(3) Bei Ihrer Kontaktaufnahme mit uns per E-Mail werden die von Ihnen mitgeteilten Daten (Ihre E-Mail-Adresse, ggf. Ihr Name und Ihre Telefonnummer) von uns gespeichert, um Ihre Fragen zu beantworten. Die in diesem Zusammenhang anfallenden Daten löschen wir, nachdem die Speicherung nicht mehr erforderlich ist, oder schränken die Verarbeitung ein, falls gesetzliche Aufbewahrungspflichten bestehen.
(4) Falls wir für einzelne Funktionen unseres Angebots auf beauftragte Dienstleister zurückgreifen oder Ihre Daten für werbliche Zwecke nutzen möchten, werden wir Sie untenstehend im Detail über die jeweiligen Vorgänge informieren. Dabei nennen wir auch die festgelegten Kriterien der Speicherdauer.
(5) Um unberechtigte Zugriffe Dritter auf Ihre persönlichen Daten, zu verhindern, wird die Verbindung per SSL/TLS-Zertifikat verschlüsselt.

#### § 2 Erhebung personenbezogener Daten bei Besuch unserer Website
Bei der bloßen informatorischen Nutzung der Website, also wenn Sie sich nicht registrieren oder uns anderweitig Informationen übermitteln, erheben wir nur die personenbezogenen Daten, die Ihr Browser an unseren Server übermittelt. Wenn Sie unsere Website betrachten möchten, erheben wir die folgenden Daten, die für uns technisch erforderlich sind, um Ihnen unsere Website anzuzeigen und die Stabilität und Sicherheit zu gewährleisten (Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. f DS-GVO). Diese allgemeinen Daten und Informationen werden in den Logfiles des Servers gespeichert:
IP-Adresse
Datum und Uhrzeit der Anfrage
Zeitzonendifferenz zur Greenwich Mean Time (GMT)
Inhalt der Anforderung (konkrete Seite)
Zugriffsstatus/HTTP-Statuscode
jeweils übertragene Datenmenge
Website, von der die Anforderung kommt
Browser
Betriebssystem und dessen Oberfläche
Sprache und Version der Browsersoftware.
Ihre IP-Adresse wird im Rahmen eines Log-Protokolls nach 6 Wochen endgültig gelöscht.
Bei der Nutzung dieser allgemeinen Daten und Informationen ziehen wir keine Rückschlüsse auf Sie. Die Daten der Server-Logfiles werden getrennt von allen durch Sie angegebenen personenbezogenen Daten gespeichert.

#### § 3 Ihre Rechte
(1) Sie haben gegenüber uns folgende Rechte hinsichtlich der Sie betreffenden personenbezogenen Daten:
- das Recht auf Auskunft nach Art. 15 DS-GVO i.V.m. § 34 BDSG.
- das Recht auf Berichtigung gemäß Art. 16 DS-GVO.
- das Recht auf Löschung aus Art. 17 DS-GVO i.V.m. § 35 BDSG.
- das Recht auf Einschränkung der Verarbeitung gemäß Art. 18 DS-GVO.
- das Recht auf Datenübertragbarkeit aus Art. 20 DS-GVO.
- das Recht auf Beschwerde bei einer zuständigen Datenschutzaufsichtsbehörde gemäß Art. 77 DS-GVO i. V. m. § 19 BDSG. Möchten Sie Ihr Recht auf Beschwerde wahrnehmen, können Sie Ihre Beschwerde an den unter Ziffer 1 genannten Datenschutzbeauftragten oder an die zuständige Datenschutzaufsichtsbehörde richten.
- das Widerspruchsrecht gemäß Art. 21 DS-GVO.


#### § 4 Widerspruch oder Widerruf gegen die Verarbeitung Ihrer Daten
(1) Falls Sie eine Einwilligung zur Verarbeitung Ihrer Daten erteilt haben, können Sie diese jederzeit widerrufen. Ein solcher Widerruf beeinflusst die Zulässigkeit der Verarbeitung Ihrer personenbezogenen Daten, nachdem Sie ihn gegenüber uns ausgesprochen haben.
(2) Soweit wir die Verarbeitung Ihrer personenbezogenen Daten auf die Interessenabwägung stützen, können Sie Widerspruch gegen die Verarbeitung einlegen. Dies ist der Fall, wenn die Verarbeitung insbesondere nicht zur Erfüllung eines Vertrags mit Ihnen erforderlich ist, was von uns jeweils bei der nachfolgenden Beschreibung der Funktionen dargestellt wird. Bei Ausübung eines solchen Widerspruchs bitten wir um Darlegung der Gründe, weshalb wir Ihre personenbezogenen Daten nicht wie von uns durchgeführt verarbeiten sollten. Im Falle Ihres begründeten Widerspruchs prüfen wir die Sachlage und werden entweder die Datenverarbeitung einstellen bzw. anpassen oder Ihnen unsere zwingenden schutzwürdigen Gründe aufzeigen, aufgrund derer wir die Verarbeitung fortführen.
(3) Selbstverständlich können Sie der Verarbeitung Ihrer personenbezogenen Daten für Zwecke der Werbung und Datenanalyse jederzeit widersprechen. Über Ihren Werbewiderspruch können Sie uns unter folgenden Kontaktdaten informieren: Op de Wisch e.V., Oberstraße 14b, 20144 Hamburg, Telefon: 040-600 88 34 00, Telefax: 040-600 88 34 22, E-Mail: info@op-de-wisch.de .

#### § 5 Automatisierte Entscheidungen im Einzelfall einschließlich Profiling
Sie haben das Recht, nicht einer ausschließlich auf einer automatisierten Verarbeitung — einschließlich Profiling — beruhenden Entscheidung unterworfen zu werden, die Ihnen gegenüber rechtliche Wirkung entfaltet oder Sie in ähnlicher Weise erheblich beeinträchtigt. Dies gilt sofern die Entscheidung nicht für den Abschluss oder die Erfüllung eines Vertrags zwischen uns erforderlich ist, oder aufgrund von Rechtsvorschriften der Union oder der Mitgliedstaaten, denen wir unterliegen, zulässig ist und diese Rechtsvorschriften angemessene Maßnahmen zur Wahrung Ihrer Rechte und Freiheiten sowie Ihrem berechtigten Interesse enthalten oder mit Ihrer ausdrücklicher Einwilligung erfolgt.
Ist die Entscheidung für den Abschluss oder die Erfüllung eines Vertrags erforderlich oder erfolgt sie aufgrund ausdrücklicher Einwilligung, treffen wir angemessene Maßnahmen, um Ihre Rechte und Freiheiten sowie Ihr berechtigtes Interesse zu wahren, wozu mindestens das Recht auf Erwirkung des Eingreifens einer Person seitens des Verantwortlichen, auf Darlegung des eigenen Standpunkts und auf Anfechtung der Entscheidung gehört.
Möchten Sie Rechte mit Bezug auf automatisierte Entscheidungen geltend machen, können Sie sich hierzu jederzeit an uns wenden.
Als verantwortungsbewusstes Unternehmen verzichten wir auf eine automatische Entscheidungsfindung oder ein Profiling.

#### § 6 Speicherdauer
Sofern nicht in dem jeweiligen Abschnitt dieser Datenschutzerklärung explizit die Löschungsfristen bestimmter Daten geregelt ist, werden wir Ihre personenbezogenen Daten gemäß den jeweiligen gesetzlichen Aufbewahrungsfristen löschen oder wenn der Speicherzweck weggefallen ist. Nach Ablauf der Frist werden die Daten routinemäßig gelöscht, sofern sie nicht mehr zur Vertragserfüllung oder Vertragsanbahnung erforderlich sind.

#### § 7 Weitere Funktionen und Angebote unserer Website
(1) Neben der rein informatorischen Nutzung unserer Website bieten wir verschiedene Leistungen an, die Sie bei Interesse nutzen können. Dazu müssen Sie in der Regel weitere personenbezogene Daten angeben, die wir zur Erbringung der jeweiligen Leistung nutzen und für die die zuvor genannten Grundsätze zur Datenverarbeitung gelten.
(2) Teilweise bedienen wir uns zur Verarbeitung Ihrer Daten externer Dienstleister. Diese wurden von uns sorgfältig ausgewählt und beauftragt, sind an unsere Weisungen gebunden und werden regelmäßig kontrolliert.
(3) Weiterhin können wir Ihre personenbezogenen Daten an Dritte weitergeben, wenn Aktionsteilnahmen, Vertragsabschlüsse oder ähnliche Leistungen von uns gemeinsam mit Partnern angeboten werden. Nähere Informationen hierzu erhalten Sie bei Angabe Ihrer personenbezogenen Daten oder in der Beschreibung des Angebotes.
(4) Soweit unsere Dienstleister oder Partner ihren Sitz in einem Staat außerhalb des Europäischen Wirtschaftsraumen (EWR) haben, informieren wir Sie über die Folgen dieses Umstands in der Beschreibung des Angebotes.

#### § 8 Externe Links
Wir verwenden auf unserer Website Links, die zu anderen Websites führen (externe Links), diese sind besonders gekennzeichnet. Deren Inhalte befinden sich nicht auf unserem Server. Die externen Inhalte dieser Links wurden beim Setzen der Links geprüft. Es kann jedoch nicht ausgeschlossen werden, dass die Inhalte von den jeweiligen Anbietern nachträglich verändert wurden. Sollten Sie bemerken, dass die Inhalte der externen Anbieter gegen geltendes Recht verstoßen, teilen Sie uns dies bitte mit. Diese Datenschutzerklärung gilt nur für Inhalte auf unserer Website.

#### § 9 Nutzung von Cookies
(1) Zusätzlich zu den zuvor genannten Daten werden bei Ihrer Nutzung unserer Website Cookies auf Ihrem Rechner gespeichert. Bei Cookies handelt es sich um kleine Textdateien, die auf Ihrer Festplatte dem von Ihnen verwendeten Browser zugeordnet gespeichert werden und durch welche der Stelle, die den Cookie setzt (hier durch uns), bestimmte Informationen zufließen. Cookies können keine Programme ausführen oder Viren auf Ihren Computer übertragen. Sie dienen dazu, das Internetangebot insgesamt nutzerfreundlicher und effektiver zu machen.
(2) Einsatz von Cookies:
a)	Diese Website nutzt folgende Arten von Cookies, deren Umfang und Funktionsweise im Folgenden erläutert werden:
Transiente Cookies (dazu b)
Persistente Cookies (dazu c).
b)	Transiente Cookies werden automatisiert gelöscht, wenn Sie den Browser schließen. Dazu zählen insbesondere die Session-Cookies. Diese speichern eine sogenannte Session-ID, mit welcher sich verschiedene Anfragen Ihres Browsers der gemeinsamen Sitzung zuordnen lassen. Dadurch kann Ihr Rechner wiedererkannt werden, wenn Sie auf unsere Website zurückkehren. Die Session-Cookies werden gelöscht, wenn Sie sich ausloggen oder den Browser schließen.
c)	Persistente Cookies werden automatisiert nach einer vorgegebenen Dauer gelöscht, die sich je nach Cookie unterscheiden kann. Sie können die Cookies in den Sicherheitseinstellungen Ihres Browsers jederzeit löschen.
d) Sie können Ihre Browser-Einstellung entsprechend Ihren Wünschen konfigurieren und z. B. die Annahme von Third-Party-Cookies oder allen Cookies ablehnen. Wir weisen Sie darauf hin, dass Sie eventuell nicht alle Funktionen dieser Website nutzen können.

#### § 10 Einbindung von Google Maps
(1) Auf dieser Webseite nutzen wir das Angebot von Google Maps. Dadurch können wir Ihnen interaktive Karten direkt in der Website anzeigen und ermöglichen Ihnen die komfortable Nutzung der Karten-Funktion.
(2)Durch den Besuch auf der Website erhält Google die Information, dass Sie die entsprechende Unterseite unserer Website aufgerufen haben. Zudem werden die unter § 2 dieser Erklärung genannten Daten übermittelt. Dies erfolgt unabhängig davon, ob Google ein Nutzerkonto bereitstellt, über das Sie eingeloggt sind, oder ob kein Nutzerkonto besteht. Wenn Sie bei Google eingeloggt sind, werden Ihre Daten direkt Ihrem Konto zugeordnet. Wenn Sie die Zuordnung mit Ihrem Profil bei Google nicht wünschen, müssen Sie sich vor Aktivierung des Buttons ausloggen. Google speichert Ihre Daten als Nutzungsprofile und nutzt sie für Zwecke der Werbung, Marktforschung und/oder bedarfsgerechten Gestaltung seiner Website. Eine solche Auswertung erfolgt insbesondere (selbst für nicht eingeloggte Nutzer) zur Erbringung von bedarfsgerechter Werbung und um andere Nutzer des sozialen Netzwerks über Ihre Aktivitäten auf unserer Website zu informieren. Ihnen steht ein Widerspruchsrecht zu gegen die Bildung dieser Nutzerprofile, wobei Sie sich zur Ausübung dessen an Google richten müssen.
(3) Weitere Informationen zu Zweck und Umfang der Datenerhebung und ihrer Verarbeitung durch den Plug-in-Anbieter erhalten Sie in den Datenschutzerklärungen des Anbieters. Dort erhalten Sie auch weitere Informationen zu Ihren diesbezüglichen Rechten und Einstellungsmöglichkeiten zum Schutze Ihrer Privatsphäre: http://www.google.de/intl/de/policies/privacy . Google verarbeitet Ihre personenbezogenen Daten auch in den USA und hat sich dem EU-US Privacy Shield unterworfen, https://www.privacyshield.gov/EU-US-Framework .

#### § 11 Einsatz von Google Analytics
(1) Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Website, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Website-Aktivitäten zusammenzustellen und um weitere mit der Website-Nutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Website-Betreiber zu erbringen.
(2) Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.
(3) Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plug-in herunterladen und installieren: http://tools.google.com/dlpage/gaoptout?hl=de.
(4) Diese Website verwendet Google Analytics mit der Erweiterung „_anonymizeIp()“. Dadurch werden IP-Adressen gekürzt weiterverarbeitet, eine Personenbeziehbarkeit kann damit ausgeschlossen werden. Soweit den über Sie erhobenen Daten ein Personenbezug zukommt, wird dieser also sofort ausgeschlossen und die personenbezogenen Daten damit umgehend gelöscht.
(5) Wir nutzen Google Analytics, um die Nutzung unserer Website analysieren und regelmäßig verbessern zu können. Über die gewonnenen Statistiken können wir unser Angebot verbessern und für Sie als Nutzer interessanter ausgestalten. Für die Ausnahmefälle, in denen personenbezogene Daten in die USA übertragen werden, hat sich Google dem EU-US Privacy Shield unterworfen, https://www.privacyshield.gov/EU-US-Framework . Rechtsgrundlage für die Nutzung von Google Analytics ist Art. 6 Abs. 1 S. 1 lit. f DS-GVO.
(6) Informationen des Drittanbieters: Google Dublin, Google Ireland Ltd., Gordon House, Barrow Street, Dublin 4, Ireland, Fax: +353 (1) 436 1001. Nutzerbedingungen: http://www.google.com/analytics/terms/de.html , Übersicht zum Datenschutz: http://www.google.com/intl/de/analytics/learn/privacy.html , sowie die Datenschutzerklärung: http://www.google.de/intl/de/policies/privacy .
