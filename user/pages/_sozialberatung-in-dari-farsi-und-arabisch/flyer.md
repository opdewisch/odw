---
title: 'Sozialberatung in Dari/Farsi und Arabisch'
date: '11-06-2018 10:35'
---

Wir bieten ab sofort unsere Sozialberatung an unserem Standort Mitte in der Grootsruhe 2 auch in den Sprachen Dari/Farsi sowie Arabisch an. Jeden ersten Donnerstag im Monat von 12:00 bis 14:00 Uhr in Dari/Farsi und jeden 3. Donnerstag zur gleichen Zeit in Arabisch. Alle aktuellen Termine finden sich im Wochenprogamm der Begegnungsstätte Mitte und alle Infos zu Ort und Adresse unter Standort Mitte auf unserer Homepage. 