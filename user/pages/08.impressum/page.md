---
title: Impressum
metadata:
    description: 'Das ist das Impressum von Op de Wisch. '
    keywords: Impressum
footerPage: true
headerImageAlt: Impressum
pageColor: grey
headerImage:
    user/pages/08.impressum/Impressum_Header_1920x560px.jpg:
        name: Impressum_Header_1920x560px.jpg
        type: image/jpeg
        size: 1457994
        path: user/pages/08.impressum/Impressum_Header_1920x560px.jpg
---

# IMPRESSUM
**Op de Wisch e.V.**<br>
<br>
Oberstraße 14b<br>
20144 Hamburg<br>
Telefon: <a href="tel:+4940600883400">040_600 88 34 00</a><br>
Telefax: 040_600 88 34 22<br>
E-Mail: <a href="mailto:info@op-de-wisch.de">info@op-de-wisch.de</a>

#### Vertreten durch:
Ute Peters

#### Registereintrag:
Eingetragen im Vereinsregister.<br>
Registergericht: Hamburg<br>
Registernummer: VR12659

#### Konzeption & Gestaltung
Brigitte Krainer<br>
<a href="http://www.krainerei.de" target="_blank">www.krainerei.de</a>

#### Technische Umsetzung
Robert Lewinske<br>
<a href="http://www.geteiltdurchnull.de" target="_blank">www.geteiltdurchnull.de</a><br>
+<br>
Sebastian Rühmann<br>
<a href="http://www.sebastianruehmann.de" target="_blank">www.sebastianruehmann.de</a>

#### Bildnachweise
Heike Günther<br>
<a href="http://www.heikeguenther.de" target="_blank">www.heikeguenther.de</a><br>
+<br>
Sebastian Engels<br>
<a href="http://www.sebastian-engels.de" target="_blank">www.sebastian-engels.de</a><br>
+<br>
Shutterstock<br>
<a href="http://www.shutterstock.com" target="_blank">www.shutterstock.com</a><br>
`#` 190783718 © KonstantinChristian<br>
`#` 212819218 © Matej Kastelic<br>
`#` 285863000 © Tyler Olson<br>
`#` 285863000 © Tyler Olson<br>
`#` 523536859 © Rawpixel.com<br>
`#` 314438072 © ldutko