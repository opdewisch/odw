---
title: 'Stundenplan Eimsbüttel'
published: true
never_cache_twig: true
cache_enable: false
file:
    user/pages/06.programm/02.eimsbuettel/_stundenplan-eimsbuettel/Internet_Angebote_Übersicht_Eimsbüttel.csv:
        name: Internet_Angebote_Übersicht_Eimsbüttel.csv
        type: application/vnd.ms-excel
        size: 1443
        path: user/pages/06.programm/02.eimsbuettel/_stundenplan-eimsbuettel/Internet_Angebote_Übersicht_Eimsbüttel.csv
---
