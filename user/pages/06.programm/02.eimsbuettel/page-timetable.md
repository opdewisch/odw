---
title: Eimsbüttel
expires: 'no cache'
never_cache_twig: true
cache_enable: false
metadata:
    description: 'Das ist das Programm unserer Begegnungsstätte Eimsbuettel.'
    keywords: 'Woche, Programm, Begegnungsstätte, Eimsbüttel, Schwimmen, Malgruppe, Stabi-Gruppe, Frühstück, Kochgruppe, Yoga-Gruppe, Peer-Beratung'
wrapperBackground: true
headerImageAlt: 'Frisches Programm und gemeinsames offenes Frühstück'
pageColor: blue
headerImage:
    user/pages/06.programm/02.eimsbuettel/Programm_Eimsbuettel_Header_1920x560px.jpg:
        name: Programm_Eimsbuettel_Header_1920x560px.jpg
        type: image/jpeg
        size: 590810
        path: user/pages/06.programm/02.eimsbuettel/Programm_Eimsbuettel_Header_1920x560px.jpg
---

<a class="button button--location" href="/standorte/eimsbuettel">Zum Standort <img src="{{ theme_url }}/images/icons/OdW_Icon_ZumStandort_30x30px.svg" alt="Standort Eimbsbüttel" /></a>
## Wochenplan für den Standort Eimsbüttel
{{ page.children|first.content }}
