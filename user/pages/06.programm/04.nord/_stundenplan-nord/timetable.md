---
title: 'Stundenplan Nord'
never_cache_twig: true
cache_enable: false
published: true
file:
    user/pages/06.programm/04.nord/_stundenplan-nord/Internet_Angebote_Übersicht_Nord.csv:
        name: Internet_Angebote_Übersicht_Nord.csv
        type: application/vnd.ms-excel
        size: 1448
        path: user/pages/06.programm/04.nord/_stundenplan-nord/Internet_Angebote_Übersicht_Nord.csv
---
