---
title: Nord
expires: 'no cache'
never_cache_twig: true
cache_enable: false
wrapperBackground: true
headerImageAlt: 'Kreatives Programm'
pageColor: blue
headerImage:
    user/pages/06.programm/04.nord/Programm_Nord_Header_1920x560px.jpg:
        name: Programm_Nord_Header_1920x560px.jpg
        type: image/jpeg
        size: 704053
        path: user/pages/06.programm/04.nord/Programm_Nord_Header_1920x560px.jpg
---

<a class="button button--location" href="/standorte/nord">Zum Standort <img src="{{ theme_url }}/images/icons/OdW_Icon_ZumStandort_30x30px.svg" alt="Standort Nord" /></a>
## Wochenplan für den Standort Nord
{{ page.children|first.content }}
