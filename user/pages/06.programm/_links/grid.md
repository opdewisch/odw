---
title: Verlinkungen
itemsPerRow: 3
---

<div class="teaserbox">
  <a class="blue" href="/programm/altona">
    <div class="teaserbox-hl">Altona</div>
    {{ page.media['Programm_Link_Altona_520x120px.jpg'].html('','Programm Altona') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="blue" href="/programm/eimsbuettel">
    <div class="teaserbox-hl">Eimsbüttel</div>
    {{ page.media['Programm_Link_Eimsbuettel_520x120px.jpg'].html('','Programm Eimsbüttel') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="blue" href="/programm/mitte">
    <div class="teaserbox-hl">Mitte</div>
    {{ page.media['Programm_Link_Mitte_520x120px.jpg'].html('','Programm Mitte') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="blue" href="/programm/nord">
    <div class="teaserbox-hl">Nord</div>
    {{ page.media['Programm_Link_Nord_520x120px.jpg'].html('','Programm Nord') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="blue" href="/programm/wandsbek">
    <div class="teaserbox-hl">Wandsbek</div>
    {{ page.media['Programm_Link_Wandsbek_520x120px.jpg'].html('','Programm Wandsbek') }}
  </a>
</div>
