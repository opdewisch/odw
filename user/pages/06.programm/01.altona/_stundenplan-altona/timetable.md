---
title: 'Stundenplan Altona'
published: true
never_cache_twig: true
cache_enable: false
file:
    user/pages/06.programm/01.altona/_stundenplan-altona/Internet_Angebote_Übersicht_Altona.csv:
        name: Internet_Angebote_Übersicht_Altona.csv
        type: application/vnd.ms-excel
        size: 1604
        path: user/pages/06.programm/01.altona/_stundenplan-altona/Internet_Angebote_Übersicht_Altona.csv
---
