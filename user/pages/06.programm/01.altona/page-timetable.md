---
title: Altona
expires: 'no cache'
never_cache_twig: true
cache_enable: false
metadata:
    description: 'Das ist das Programm unserer Begegnungsstätte Altona. '
    keywords: 'Woche, Programm, Begegnungsstätte, Altona, Stabi-Gruppe, Frauenschwimmen, Kochen, Genesungsbegleitersprechstunde, offenes Frühstück, Schwimmen, Gitarrengruppe'
wrapperBackground: true
headerImageAlt: 'Frisches Programm'
pageColor: blue
headerImage:
    user/pages/06.programm/01.altona/Programm_Altona_Header_1920x560px.jpg:
        name: Programm_Altona_Header_1920x560px.jpg
        type: image/jpeg
        size: 963226
        path: user/pages/06.programm/01.altona/Programm_Altona_Header_1920x560px.jpg
---

<a class="button button--location" href="/standorte/altona">Zum Standort <img src="{{ theme_url }}/images/icons/OdW_Icon_ZumStandort_30x30px.svg" alt="Standort Altona" /></a>
## Wochenplan für den Standort Altona
{{ page.children|first.content }}
