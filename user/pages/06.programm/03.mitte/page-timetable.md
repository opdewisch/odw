---
title: Mitte
expires: 'no cache'
never_cache_twig: true
cache_enable: false
metadata:
    description: 'Das ist das Programm unserer Begegnungsstätte Mitte. '
    keywords: 'Woche, Programm, Begegnungsstütte, Mitte, Frühstück, Resilienzgruppe, Genesungsbegleitersprechstunde, Angehörigenbegleitung, Afrikanische Gruppe, Theatergruppe, Persisch/Afghanische Gruppe'
wrapperBackground: true
headerImageAlt: 'Sportliches Programm und Entspannung'
pageColor: blue
headerImage:
    user/pages/06.programm/03.mitte/Programm_Mitte_Header_1920x560px.jpg:
        name: Programm_Mitte_Header_1920x560px.jpg
        type: image/jpeg
        size: 926234
        path: user/pages/06.programm/03.mitte/Programm_Mitte_Header_1920x560px.jpg
---

<a class="button button--location" href="/standorte/mitte">Zum Standort <img src="{{ theme_url }}/images/icons/OdW_Icon_ZumStandort_30x30px.svg" alt="Standort Mitte" /></a>
## Wochenplan für den Standort Mitte
{{ page.children|first.content }}
