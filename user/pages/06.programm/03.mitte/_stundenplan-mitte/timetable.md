---
title: 'Stundenplan Mitte'
published: true
never_cache_twig: true
cache_enable: false
file:
    user/pages/06.programm/03.mitte/_stundenplan-mitte/Internet_Angebote_Übersicht_Mitte.csv:
        name: Internet_Angebote_Übersicht_Mitte.csv
        type: application/vnd.ms-excel
        size: 1525
        path: user/pages/06.programm/03.mitte/_stundenplan-mitte/Internet_Angebote_Übersicht_Mitte.csv
---
