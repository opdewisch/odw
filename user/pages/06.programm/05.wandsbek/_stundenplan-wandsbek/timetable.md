---
title: 'Stundenplan Wandsbek'
published: true
never_cache_twig: true
cache_enable: false
file:
    user/pages/06.programm/05.wandsbek/_stundenplan-wandsbek/Internet_Angebote_Übersicht_Wandsbek.csv:
        name: Internet_Angebote_Übersicht_Wandsbek.csv
        type: application/vnd.ms-excel
        size: 1345
        path: user/pages/06.programm/05.wandsbek/_stundenplan-wandsbek/Internet_Angebote_Übersicht_Wandsbek.csv
---
