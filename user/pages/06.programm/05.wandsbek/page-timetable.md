---
title: Wandsbek
expires: 'no cache'
never_cache_twig: true
cache_enable: false
metadata:
    description: 'Das ist das Programm unserer Begegnungsstätte Wandsbek. '
    keywords: 'Woche, Programm, Begegnungsstätte, Wandsbek, Zeitungsgruppe, Literaturgruppe, Rhythm Fever, Tischtennis, Debattier-Club'
wrapperBackground: true
headerImageAlt: 'Interkulturelles Programm'
pageColor: blue
headerImage:
    user/pages/06.programm/05.wandsbek/Programm_Wandsbek_Header_1920x560px.jpg:
        name: Programm_Wandsbek_Header_1920x560px.jpg
        type: image/jpeg
        size: 858981
        path: user/pages/06.programm/05.wandsbek/Programm_Wandsbek_Header_1920x560px.jpg
---

<a class="button button--location" href="/standorte/wandsbek">Zum Standort <img src="{{ theme_url }}/images/icons/OdW_Icon_ZumStandort_30x30px.svg" alt="Standort Wandsbek" /></a>
## Wochenplan für den Standort Wandsbek
{{ page.children|first.content }}
