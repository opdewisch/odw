---
title: Programm
metadata:
    description: 'Wir bieten in unseren 5 Begegnungsstätten ein abwechslungsreiches wöchentliches Programm.'
    keywords: 'Woche, Programm, Begegnungsstätte, Angebot, Angebotsberatung, Sozialberatung, Genesungsbegleitersprechstunde'
wrapperBackground: false
headerImageAlt: 'Neue Wege gehen'
pageColor: blue
headerImage:
    user/pages/06.programm/Programm_Header_1920x560px.jpg:
        name: Programm_Header_1920x560px.jpg
        type: image/jpeg
        size: 928748
        path: user/pages/06.programm/Programm_Header_1920x560px.jpg
navigationIcon:
    user/pages/06.programm/OdW_Icon_Programm_170x125px_AKTIV.svg:
        name: OdW_Icon_Programm_170x125px_AKTIV.svg
        type: image/svg+xml
        size: 2465
        path: user/pages/06.programm/OdW_Icon_Programm_170x125px_AKTIV.svg
content:
    items: '@self.modular'
    order:
        by: header.news_date
        dir: asc
---

## Unser Wochenprogramm - wir laden Sie ein!
In jeder unserer fünf Begegnungsstätten bieten wir Ihnen wöchentlich stattfindende, abwechslungsreiche Angebote. An einigen Angeboten können Sie ohne Anmeldung teilnehmen, andere Angebote erfordern eine Anmeldung und ggf. eine kleine Zuzahlung.
Bitte schauen Sie sich in Ruhe die Legende zum Wochenplan an, denn dort wird alles genau erklärt.
Generell stehen Ihnen alle Angebote offen unabhängig davon, ob es sich um eine Begegnungsstätte in Ihrer direkten Wohnumgebung handelt. Welche Programme die einzelnen Begegnungsstätten für Sie bereithalten sehen Sie hier.
<br>
<br>

{% for module in page.collection() %}
{{ module.content }}
{% endfor %}
