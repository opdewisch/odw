---
title: Interkulturelles
metadata:
    description: 'Multikulturelle Teams bilden die Basis für verschiedene interkulturelle Angebote, wie z.B. Afrikanisches Trommeln oder internationales Kochen. '
    keywords: 'Multikulturell, Kultur, Inklusion, Migration'
wrapperBackground: true
headerImageAlt: 'Treffpunkt der Kulturen'
pageColor: lightgreen
headerImage:
    user/pages/05.projekte/03.interkulturelles/Projekte_Interkulturelles_Header_1920x560px.jpg:
        name: Projekte_Interkulturelles_Header_1920x560px.jpg
        type: image/jpeg
        size: 794720
        path: user/pages/05.projekte/03.interkulturelles/Projekte_Interkulturelles_Header_1920x560px.jpg
---

<div class="box">
<h2>Treffpunkt der Kulturen</h2>
<p>Unsere multi-kulturellen und damit auch multi-muttersprachlichen Teams, die zum Teil traumapädagogisch ausgebildet sind, bilden die im wahrsten Sinne des Wortes verständnisvolle Basis für einen interkulturellen Austausch. Hier begegnen sich Menschen mit unterschiedlichen kulturellen Prägungen und Werten. Dies gelingt umso besser, wenn der gemeinsame Nenner die Bereitschaft ist, sich vorurteilsfrei auf andere einzulassen und zu lernen, was die jeweilige Art des Wahrnehmens, Fühlens oder Bewertens bestimmt. Diese offene Haltung im Miteinander finden Sie in den Begegnungsstätten von Op de Wisch.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['Projekte_Interkulturelles_Content_1_810x400px.jpg'].html('','Felicia') }}
        {{ page.media['Projekte_Interkulturelles_Content_2_810x400px.jpg'].html('','Trommelgruppe') }}
        {{ page.media['Projekte_Interkulturelles_Content_3_810x400px.jpg'].html('','International') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Angebote wie diese stehen für Inklusion</h2>
	<p>Das bedeutet für die Praxis: Wir begegnen Ihnen auf Augenhöhe und nehmen Sie an, wie Sie sind – unabhängig von Merkmalen wie Geschlecht, Alter, Herkunft, Bildungsstand, sexueller Neigungen oder möglicher Behinderungen. Denn normal ist vor allem eins: unsere Unterschiedlichkeit.</p>
	<p>Im bunten Treiben unserer Begegnungsstätten finden auch interkulturelle Gruppenangebote statt, die das Gemeinschaftsgefühl fördern, wie z.B. rhythmisch-meditativ beim Trommeln oder mit der richtigen Würze beim internationalen Kochen.</p>
    </div>
</div>
