---
title: 'Reit- und Naturprojekt'
metadata:
    description: 'Unsere eigene Reitanlage mit Garten ist ein Ort für die Sinne. Der Umgang mit dem Pferd und das Reiten wirken sich positiv auf Körper und Psyche aus. '
    keywords: 'Heilpädagogisches Reiten, sozialintegratives Reiten, Gudrun Sailer-Maaß, Feldenkrais, Natur, tiergestützte Pädagogik'
wrapperBackground: true
headerImageAlt: 'Mensch und Pferd'
pageColor: lightgreen
headerImage:
    user/pages/05.projekte/02.projekt-reit-und-naturprojekt/Projekte_ReitNaturprojekt_Header_1920x560px.jpg:
        name: Projekte_ReitNaturprojekt_Header_1920x560px.jpg
        type: image/jpeg
        size: 939970
        path: user/pages/05.projekte/02.projekt-reit-und-naturprojekt/Projekte_ReitNaturprojekt_Header_1920x560px.jpg
---

<div class="box">
<h2>Balsam für die Seele: Reiten und Natur</h2>
<p>Op de Wisch hält für Menschen mit psychischen Problemen und im Rahmen der Jugendhilfe ein ganz besonderes Angebot bereit: Soziale Integration und Kompetenzerweiterung mit Hilfe von Pferden.</p>
<p>Wenn wir Ihnen "einen vom Pferd erzählen", dann wollen wir Ihnen keine Märchen präsentieren, sondern auf unsere Reitanlage im Nordosten Hamburgs aufmerksam machen. Angrenzend an das Naherholungsgebiet Duvenstedter Brook haben wir ein Naturidyll mit Garten, das sich Pferde, Katzen und Hühner teilen. Es ist ein Ort der Begegnung mit zwangloser Atmosphäre, der ideal ist, um sich Selbst und den eigenen Gefühlen näher zu kommen.</p>
<p>Unser Team aus ReitpädagogInnen und –therapeutInnen arbeitet mit der Feldenkrais-Methode, nutzt  Elemente des Centered Riding und verfügt insbesondere über fundierte Kenntnisse und Erfahrungen in der traumapädagogischen Arbeit. Unter der Leitung von Gudrun Sailer-Maaß arbeiten wir mit Kindern und Erwachsenen und bieten sowohl sozialintegratives als auch heilpädagogisches Reiten an.</p>
<p>Unser großer Dank gilt den Förderern unseres Reit- und Naturprojektes, dem Hamburger Spendenparlament, Radio Hamburg, dem Lionsclub, Frau Triborski, dem Hamburger Abendblatt und DATEV.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['Projekte_ReitNaturprojekt_Content_1_810x400px.jpg'].html('','Team Reitpaedagogen Claudia Behnke, Gudrun Sailer-Maaß') }}
        {{ page.media['Projekte_ReitNaturprojekt_Content_2_810x400px.jpg'].html('','Pferd wartet auf Einsatz im Reitprojekt') }}
        {{ page.media['Projekte_ReitNaturprojekt_Content_3_810x400px.jpg'].html('','Entspannung im Reitprojekt') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Die Heilsame Wirkung der Arbeit mit dem Pferd in der Natur</h2>
	<p>Der Kontakt zum Pferd - und natürlich auch zu unseren anderen Tieren - soll vor allem das allgemeine Wohlbefinden und die soziale Integration fördern. Damit geht oft auch eine Steigerung der Beziehungsfähigkeit und des Selbstwertgefühls einher.  Die gesundheitsfördernde Wirkung von Tieren und der Natur werden bei uns genutzt, um den Menschen wieder ins Gleichgewicht zu bringen. Die Arbeit mit den Pferden wirkt stressreduzierend,  fördert eigene Kompetenzen und lehrt eigene Gefühle wiederzuentdecken und mit Affekten adäquater umzugehen.<br>
Mit Hilfe der ganzheitlichen, körperbezogenen Lernmethode nach Moshe Feldenkrais werden die Selbstwahrnehmung geschult und das Körpergefühl verbessert. Gemäß des Leitsatzes „Wenn Du weißt was Du tust, kannst Du tun was Du willst." (Moshe Feldenkrais) ermöglicht die Methode eine Veränderung im Denken, Wahrnehmen und Handeln. Dies führt zu psychischer Stabilität und zu einer Förderung der Selbständigkeit.
In der Interaktion mit den Pferden in der freien Natur erleben Sie unter fachlicher Anleitung erfahrener Reitpädagogen einen neuen Zugang zu sich selbst. Diese Erfahrungen können helfen, Blockaden zu lösen und sich selbst neue Wege zu eröffnen. Sich ein Stück des Lebenswegs „tragen zu lassen“, ist eine sehr heilsame Erfahrung.</p>
    </div>
</div>
<hr>
<div class="grid box">
    <div class="col-6 left col-sm-12">
    <h2>Heilpädagogisches Reiten</h2>
	<p>Wir bieten heilpädagogisches Reiten sowohl im Rahmen des Kinder- und Jugendhilfegesetzes KJHG als auch für geistig und/oder körperlich beeinträchtigte Kinder an.  Wir unterstützen z.B. bei folgenden Schwerpunkten:</p>
    <ul>
    <li>Schulversagen bis hin zur Diagnose der  Lernunfähigkeit</li>
    <li>nicht „gruppenfähig“</li>
    <li>Hyperaktivität</li>
	<li>Ängsten und den daraus resultierenden Einschränkungen</li>
	<li>Koordinations- und Wahrnehmungsstörungen</li>
	<li>Störung der Affektkontrolle/Aggressives Verhalten gegen sich und/oder andere</li>
	<li>Introvertiertheit – völliges Abblocken der Reize und Angebote des Lebensumfeldes</li>
	<li>verminderte Sinnesaktivität</li>
    </ul>
	<p>Nehmen Sie Kontakt mit uns auf, damit wir gemeinsam herausfinden, wie wir Ihre Kinder am besten unterstützen können. Wir freuen uns auf Sie und Ihr Kind - oder die ganze Familie!</p>
    </div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['Projekte_ReitNaturprojekt_Content_4_810x400px.jpg'].html('','Selbständig Pferde versorgen') }}
    	{{ page.media['Projekte_ReitNaturprojekt_Content_5_810x400px.jpg'].html('','Heilpädagogisches Reiten') }}
    </div>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
        {{ page.media['Projekte_ReitNaturprojekt_Content_6_810x400px.jpg'].html('','Sozialintegratives Reiten') }}
        {{ page.media['Projekte_ReitNaturprojekt_Content_7_810x400px.jpg'].html('','Pferde versorgen im Reitprojekt') }}
        {{ page.media['Projekte_ReitNaturprojekt_Content_6_810x400px_0218.jpg'].html }}
           </div>
    <div class="col-6 right col-sm-12">
    <h2>Sozialintegratives Reiten</h2>
	<p>Das sozialintegrative Reiten für Erwachsene ermöglicht vielfältige Erfahrungen mit dem Pferd in der Natur. Oft ermöglicht die Kontaktaufnahme zum Pferd erst die Kontaktaufnahme zu anderen Menschen. Das Reiten und die Beschäftigung mit dem Pferd haben viele heilende Auswirkungen auf den Körper und die Psyche.</p>
    <p>Es lassen sich nicht alle aufzählen, aber die Wichtigsten haben wir hier zusammengestellt:</p>
<ul>
<li>Verbesserung des Gleichgewichts und der Koordination</li>
<li>Lockerung und Festigung der Muskulatur</li>
<li>Erweiterung des Bewegungsapparates</li>
<li>Ausbildung der Sinneswahrnehmung</li>
<li>Körperausrichtung und Körperhaltung</li>
<li>Entspannung</li>
<li>Steigerung von Selbstvertrauen, Selbstständigkeit und der Fähigkeit, Verantwortung zu übernehmen</li>
<li>Stressbewältigung</li>
<li>Reduzierung von Traumafolgen</li>
<li>Öffnen für Beziehungsangebote</li>
<li>Erfahrung von Selbstwirksamkeit</li>
<li>Stabilisierung</li>
</ul>
<p>Unser Angebot richtet sich an alle Menschen, die unter psychischen Erkrankungen leiden, körperlich eingeschränkt sind, Lernschwierigkeiten haben, von Ängsten beherrscht und eingeschränkt werden oder von sozialer Isolation betroffen sind. </p>
<p>Nehmen Sie Kontakt mit uns auf, damit wir gemeinsam herausfinden, wie wir Sie am besten unterstützen können. Wir freuen uns auf Sie!</p>
    </div>
</div>
<hr>
<div class="grid box">
    <div class="col-6 left col-sm-12">
    <h2>So erreichen Sie uns</h2>
	<p>Op de Wisch e.V.<br>
	Oberstraße 14b<br>
	20144 Hamburg<br>
    </p>
    <p>Telefon <a href="tel:+4940600883400">040_600 88 34 00</a><br>
    Telefax 040_600 88 34 22<br>
    <a href="mailto:info@op-de-wisch.de">info@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
	<p>Gudrun Sailer-Maaß<br>
    Projektleiterin Reitprojekt<br>
    Mobil <a href="tel:+491704520535">0170_452 05 35</a><br>
	<a href="mailto:sailer@op-de-wisch.de">sailer@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
    </div>
    <div class="col-6 right col-sm-12">
        {{ page.media['Projekte_ReitNaturprojekt_Content_8_810x400px.jpg'].html('','Teamleiterin Gudrun Sailer-Maaß') }}
    </div>
</div>
