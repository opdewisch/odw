---
title: Zeitung
metadata:
    description: 'Hier steht geschrieben, was Klienten und Mitarbeiter gedruckt sehen wollen: Einblicke_Ausblicke erscheint vierteljährlich und wird von Klienten und Mitarbeitern herausgebracht. '
    keywords: 'Einblicke_Ausblicke, Zeitung, Peter Houffouet'
wrapperBackground: true
headerImageAlt: 'Zeitung machen'
pageColor: lightgreen
headerImage:
    user/pages/05.projekte/zeitung/Projekte_Zeitung_Header_1920x560px.jpg:
        name: Projekte_Zeitung_Header_1920x560px.jpg
        type: image/jpeg
        size: 602067
        path: user/pages/05.projekte/zeitung/Projekte_Zeitung_Header_1920x560px.jpg
---

<div class="box">
<h2>Unser Zeitungsprojekt mit KlientInnen</h2>
<p>Einblicke-Ausblicke ist eine vierteljährlich bei Op de Wisch erscheinende Zeitung, die von Klienten und Mitarbeitern herausgegeben wird. Sie bilden gemeinsam das federführende Redaktionsteam, das die Inhalte festlegt und das Layout gestaltet.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['Projekte_Zeitung_Content_1_810x400px.jpg'].html('','Redaktionsleiter Peter Houffouet') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Jede Ausgabe bietet eine große Bandbreite an unterschiedlichsten Artikeln.</h2>
	<p>Das reicht von der Vorstellung neuer MitarbeiterInnen über aktuelle Entwicklungen bei Op de Wisch bis hin zu persönlichen Beiträgen.
Die Macher sind dabei stets offen für Anregendes aus dem Umfeld und freuen sich, wenn sie Artikel, Fragen, Wünsche und dergleichen unter redaktion@op-de-wisch.de von Ihnen erhalten. Das Team hat natürlich das letzte Wort, ob und was veröffentlicht wird.</p>
	<p>Hier finden Sie das Archiv mit zurückliegenden Ausgaben, die Sie sich jederzeit herunterladen können.</p>
    </div>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	<h2>Das Archiv</h2>
    	{{ page.children|first.content }}
    </div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['Projekte_Zeitung_Content_2_810x400px.jpg'].html('','Einblicke_Ausblicke oben') }}
        {{ page.media['Projekte_Zeitung_Content_3_810x400px.jpg'].html('','Einblicke_Ausblicke mitte') }}
        {{ page.media['Projekte_Zeitung_Content_4_810x400px.jpg'].html('','Einblicke_Ausblicke unten') }}
    </div>
</div>
