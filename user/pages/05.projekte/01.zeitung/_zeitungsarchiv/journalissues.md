---
title: Zeitungsarchiv
journals:
    -
        date: '01-08-2015 00:00'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_032015.pdf:
                name: Einblicke-Ausblicke_032015.pdf
                type: application/pdf
                size: 12785071
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_032015.pdf
    -
        date: '01-11-2015 00:00'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_042015.pdf:
                name: Einblicke-Ausblicke_042015.pdf
                type: application/pdf
                size: 9720569
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_042015.pdf
    -
        date: '01-01-2016 00:00'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_012016.pdf:
                name: Einblicke-Ausblicke_012016.pdf
                type: application/pdf
                size: 10957147
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_012016.pdf
    -
        date: '01-04-2016 00:00'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_022016.pdf:
                name: Einblicke-Ausblicke_022016.pdf
                type: application/pdf
                size: 11463849
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_022016.pdf
    -
        date: '01-08-2016 00:00'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_032016.pdf:
                name: Einblicke-Ausblicke_032016.pdf
                type: application/pdf
                size: 13157254
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_032016.pdf
    -
        date: '01-11-2016 00:00'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_042016.pdf:
                name: Einblicke-Ausblicke_042016.pdf
                type: application/pdf
                size: 9987618
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_042016.pdf
    -
        date: '01-01-2017 00:00'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_012017.pdf:
                name: Einblicke-Ausblicke_012017.pdf
                type: application/pdf
                size: 6929354
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_012017.pdf
    -
        date: '01-04-2017 10:20'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_022017.pdf:
                name: Einblicke-Ausblicke_022017.pdf
                type: application/pdf
                size: 12349966
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_022017.pdf
    -
        date: '13-11-2017 11:01'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_032017.pdf:
                name: Einblicke-Ausblicke_032017.pdf
                type: application/pdf
                size: 13427230
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_032017.pdf
    -
        date: '06-02-2018 08:33'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke_Ausblicke_042017_18.pdf:
                name: Einblicke_Ausblicke_042017_18.pdf
                type: application/pdf
                size: 10798440
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke_Ausblicke_042017_18.pdf
    -
        date: '15-05-2018 09:19'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke_Ausblicke_012018.pdf:
                name: Einblicke_Ausblicke_012018.pdf
                type: application/pdf
                size: 18239158
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke_Ausblicke_012018.pdf
    -
        date: '20-08-2018 09:16'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke_Ausblicke_082018.pdf:
                name: Einblicke_Ausblicke_082018.pdf
                type: application/pdf
                size: 12347155
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke_Ausblicke_082018.pdf
    -
        date: '03-12-2018 09:19'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_112018.pdf:
                name: Einblicke-Ausblicke_112018.pdf
                type: application/pdf
                size: 13542111
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke-Ausblicke_112018.pdf
    -
        date: '05-02-2019 08:07'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke_Ausblicke_022019.pdf:
                name: Einblicke_Ausblicke_022019.pdf
                type: application/pdf
                size: 8750535
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke_Ausblicke_022019.pdf
    -
        date: '25-04-2019 08:37'
        file:
            user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke_Ausblicke_042019.pdf:
                name: Einblicke_Ausblicke_042019.pdf
                type: application/pdf
                size: 7318831
                path: user/pages/05.projekte/01.zeitung/_zeitungsarchiv/Einblicke_Ausblicke_042019.pdf
cache_enable: false
---

{# Notice: Make sure to correct the path to the hidden journalissues in the robots.txt after moving or renaming this or parent folder #}