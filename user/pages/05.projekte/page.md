---
title: Projekte
metadata:
    description: 'Die themenbezogenen Projekte ergänzen die Gruppenangebote und stehen für Orte der Begegnung und aktives Tun.'
    keywords: 'Begegnung, Lernfelder, Entwicklung, Zeitung, Einblicke, Ausblicke, Reiten, interkulturelle Arbeit, Hamburg, HH'
headerImageAlt: 'Huhn probiert etwas Neues aus'
pageColor: lightgreen
headerImage:
    user/pages/05.projekte/Projekte_Header_1920x560px.jpg:
        name: Projekte_Header_1920x560px.jpg
        type: image/jpeg
        size: 920397
        path: user/pages/05.projekte/Projekte_Header_1920x560px.jpg
navigationIcon:
    user/pages/05.projekte/OdW_Icon_Projekte_170x125px_AKTIV.svg:
        name: OdW_Icon_Projekte_170x125px_AKTIV.svg
        type: image/svg+xml
        size: 4523
        path: user/pages/05.projekte/OdW_Icon_Projekte_170x125px_AKTIV.svg
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _links
---

## Projekte von Op de Wisch
Unsere sehr unterschiedlichen und themenbezogenen Projekte vertiefen unsere Arbeit in einzelnen Bereichen und vervollständigen bzw. schärfen unser Profil. Nachfolgend finden Sie konkrete Informationen zu den einzelnen Projekten, die generell allen Klienten offenstehen. Sprechen Sie uns gerne an, wenn Sie
mitmachen möchten.
<br>
<br>

{% for module in page.collection() %}
{{ module.content }}
{% endfor %}
