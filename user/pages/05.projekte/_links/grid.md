---
title: Verlinkungen
itemsPerRow: 3
mobileItemsPerRow: 1
---

<div class="teaserbox">
  <a class="lightgreen" href="/projekte/zeitung">
    <div class="teaserbox-hl">Zeitung</div>
    {{ page.media['Projekte_Link_Zeitung_520x120px.jpg'].html('','Zeitungsprojekt') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="lightgreen" href="/projekte/reit-und-naturprojekt">
    <div class="teaserbox-hl">Reit- und Naturprojekt</div>
    {{ page.media['Projekte_Link_ReitNaturprojekt_520x120px.jpg'].html('','Reit- und Naturprojekt') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="lightgreen" href="/projekte/interkulturelles">
    <div class="teaserbox-hl">Interkulturelles</div>
    {{ page.media['Projekte_Link_Interkulturelles_520x120px.jpg'].html('','Treffpunkt der Kulturen') }}
  </a>
</div>


