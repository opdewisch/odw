---
title: Eimsbüttel
metadata:
    description: 'Helge Thoelen und sein Team stehen als Ansprechpartner für den Bezirk Eimsbüttel zur Verfügung. '
    keywords: 'Ambulante Sozialpsychiatrie, Eimsbüttel, Helge Thoelen, Oberstraße 14b'
wrapperBackground: true
headerImageAlt: 'Begegnungsstätte Eimsbuettel'
pageColor: orange
headerImage:
    user/pages/04.standorte/02.eimsbuettel/Standorte_Eimsbuettel_Header_1920x560px.jpg:
        name: Standorte_Eimsbuettel_Header_1920x560px.jpg
        type: image/jpeg
        size: 848774
        path: user/pages/04.standorte/02.eimsbuettel/Standorte_Eimsbuettel_Header_1920x560px.jpg
---

<a class="button button--program" href="/programm/eimsbuettel">Zum Programm <img src="{{ theme_url }}/images/icons/OdW_Icon_ZumProgramm_30x30px.svg" alt="Programm Eimsbüttel" /></a>
<div class="box">
<h2>Wir freuen uns, dass Sie sich für unsere Begegnungsstätte Eimsbüttel interessieren!</h2>
<p>Wir befinden uns im Erdgeschoss eines der Grindelhochhäuser. Bei Fragen zu unserem Angebot stehen wir Ihnen als Ansprechpartner für den Bezirk Hamburg-Eimsbüttel zur Verfügung.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['Angelika Mueller.jpg'].html('','Bezirksleitung Angelika Müller') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Unsere Bezirksleiterin Angelika Müller</h2>
	<p>und ihr Team werden Sie kompetent über Op de Wisch informieren und Ihnen die unterschiedlichen Hilfsangebote vorstellen. Wenn Sie eine Hilfe in Anspruch nehmen wollen, unterstützen wir Sie selbstverständlich im Antragsverfahren.</p>
	<p>Wir bieten ein vielfältiges Gruppenangebot, ausgerichtet an den Interessen der Menschen, die zu uns kommen - und offen für Besucher.</p>
	<p>Unsere Kooperationspartner im Stadtteil sind Compass – Sozialpsychiatrische Dienste GmbH, die iv hh-west gmbh und die Interkulturelle Begegnungsstätte e.V. (IKB).</p>
    </div>
</div>
<hr>
<div class="grid box">
    <div class="col-6 left col-sm-12">
    <h2>So erreichen Sie uns</h2>
	<p>Op de Wisch e.V.<br>
	Oberstraße 14b<br>
	20144 Hamburg<br>
    </p>
    <p>Telefon <a href="tel:+4940600883450">040_600 88 34 50</a><br>
    Telefax 040_600 88 34 55<br>
    <a href="mailto:info@op-de-wisch.de">info@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
	<p>Angelika Müller<br>
    Bezirksleitung Eimsbüttel<br>
    Mobil <a href="tel:+4915904475913">0159_044 75913</a><br>
    <a href="mailto:thoelen@op-de-wisch.de">mueller@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
    <p>Öffnungszeiten:<br>
    Montag 17:00 bis 20:00 Uhr<br>
	Dienstag 13:00 bis 16:00 Uhr<br>
	Mittwoch 15:00 bis 18:00 Uhr<br>
    Donnerstag 9:45 bis 15:00 Uhr<br>
	Freitag 15:00 bis 19:00 Uhr</p>

    <a class="button button--bus" href="http://www.hvv.de/fp.php?id=becdc4c2796f7b474529c37edf9f5c6f" target="_blank">{{ page.media['hvv_logo.svg'].html('','Mit dem HVV zu uns') }}</a>
    </div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['Standorte_Eimsbuettel_Content_3_810x400px.jpg'].html('','Begegnungsstaette Altona') }}
        {{ page.media['Standorte_Eimsbuettel_Content_4_810x400px.jpg'].html('','Begegnungsstaette Altona') }}
    </div>
</div>
<hr>
<div class="box">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2368.934634969439!2d9.976811316000337!3d53.576783165126464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b18f4a090657a7%3A0x83362f962bc4c409!2sOberstra%C3%9Fe+14B%2C+20144+Hamburg!5e0!3m2!1sde!2sde!4v1495795646036" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
