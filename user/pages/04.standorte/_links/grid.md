---
title: Verlinkungen
itemsPerRow: 3
---

<div class="teaserbox">
  <a class="orange" href="/standorte/altona">
    <div class="teaserbox-hl">Altona</div>
    {{ page.media['Standorte_Link_Altona_520x120px.jpg'].html('','Standort Altona') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="orange" href="/standorte/eimsbuettel">
    <div class="teaserbox-hl">Eimsbüttel</div>
    {{ page.media['Standorte_Link_Eimsbuettel_520x120px.jpg'].html('','Standort Eimsbüttel') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="orange" href="/standorte/mitte">
    <div class="teaserbox-hl">Mitte</div>
    {{ page.media['Standorte_Link_Mitte_520x120px.jpg'].html('','Standort Mitte') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="orange" href="/standorte/nord">
    <div class="teaserbox-hl">Nord</div>
    {{ page.media['Standorte_Link_Nord_520x120px.jpg'].html('','Standort Nord') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="orange" href="/standorte/wandsbek">
    <div class="teaserbox-hl">Wandsbek</div>
    {{ page.media['Standorte_Link_Wandsbek_520x120px.jpg'].html('','Standort Wandsbek') }}
  </a>
</div>
