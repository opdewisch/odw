---
title: Altona
metadata:
    description: 'Anna Beck und ihr Team stehen als Ansprechpartner für den Bezirk Altona zur Verfügung. '
    keywords: 'Ambulante Sozialpsychiatrie, Altona, Anna Beck, Eimsbütteler Straße 93-95'
wrapperBackground: true
headerImageAlt: 'Begegnungsstätte Altona'
pageColor: orange
headerImage:
    user/pages/04.standorte/01.altona/Standorte_Altona_Header_1920x560px.jpg:
        name: Standorte_Altona_Header_1920x560px.jpg
        type: image/jpeg
        size: 857136
        path: user/pages/04.standorte/01.altona/Standorte_Altona_Header_1920x560px.jpg
---

<a class="button button--program" href="/programm/altona">Zum Programm <img src="{{ theme_url }}/images/icons/OdW_Icon_ZumProgramm_30x30px.svg" alt="Programm Altona" /></a>
<div class="box">
<h2>Wir begrüßen Sie bei uns in Altona!</h2>
<p>Unsere gemütlichen Räume mit kleinem Garten liegen ganz in der Nähe vom Bahnhof Holstenstraße.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['Helga_Riess.jpg'].html('','stellvertretende Bezirksleitung Helga Riess')}} 
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Unsere Bezirksleiterin Helga Rieß</h2>
	<p>und ihr Team freuen sich über Ihren Anruf oder Besuch, wenn Sie Fragen oder Beratungsbedarf haben oder sich genauer über unsere Angebote in Altona informieren wollen. </p>
<p>Sie können uns aber auch ganz unverbindlich während unseres „offenen Treffs“ kennenlernen - immer montags zwischen 14.00-18.00 Uhr.
Zudem bieten wir unverbindliche Beratungs- und Hilfsangebote an und unterstützen Sie bei Bedarf gerne beim Antragsverfahren für eine intensivere Begleitung.
</p>
<p>Wir haben uns auf unterschiedliche Arbeitsschwerpunkte spezialisiert (siehe Wochenplan) und informieren Sie gerne darüber. Zu diesen Schwerpunkten zählen Traumaberatung, Stabilisierung und Wiedereingliederung in Arbeit- und Beschäftigung. Alle BesucherInnen und Interessierte sind uns herzlich willkommen!</p>
<p>Wir kooperieren im Stadtteil mit dem Flaks, Johanna-Ambulante Betreuung, der Vereinigung Pestalozzi, Hamburgische Brücke, Insel e.V. sowie Jugend hilft Jugend.</p>
    </div>
</div>
<hr>
<div class="grid box">
    <div class="col-6 left col-sm-12">
    <h2>So erreichen Sie uns</h2>
	<p>Op de Wisch e.V.<br>
	Eimsbütteler Straße 93-95<br>
	22769 Hamburg<br>
    </p>
    <p>Telefon <a href="tel:+494043213340">040_43 21 33 40</a><br>
    Telefax 040_43 21 37 33<br>
    <a href="mailto:info@op-de-wisch.de">info@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
	<p>Helga Rieß<br>
    Bezirksleitung Altona<br>
    Mobil <a href="tel:+4915904475900">0159_044 75 900</a><br>
	<a href="mailto:riess@op-de-wisch.de">riess@op-de-wisch.de</a></p> {# TO-BE-SECURED #}

  <a class="button button--bus" href="http://www.hvv.de/fp.php?id=643dd55e01599966d6d06f3f55a815ab" target="_blank">{{ page.media['hvv_logo.svg'].html('','Mit dem HVV zu uns') }}</a>
</div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['Standorte_Altona_Content_3_810x400px.jpg'].html('','Begegnungsstaette Altona') }}
        {{ page.media['Standorte_Altona_Content_4_810x400px.jpg'].html('','Begegnungsstaette Altona') }}
    </div>
</div>
<hr>
<div class="box">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2369.5331983489514!2d9.950891316201794!3d53.56609998002576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b18f54668b8671%3A0x1cfb451baa8c82cc!2sOp+de+Wisch+e.V.!5e0!3m2!1sen!2sde!4v1495795188765" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
