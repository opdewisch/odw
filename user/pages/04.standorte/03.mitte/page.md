---
title: Mitte
metadata:
    description: 'Anke Bamberger und ihr Team stehen als Ansprechpartner für den Bezirk Mitte zur Verfügung. '
    keywords: 'Ambulante Sozialpsychiatrie, Mitte, Anke Bamberger, Angehörigenbegleitung, Grootsruhe 2'
wrapperBackground: true
headerImageAlt: 'Begegnungsstätte Mitte'
pageColor: orange
headerImage:
    user/pages/04.standorte/03.mitte/Standorte_Mitte_Header_1920x560px.jpg:
        name: Standorte_Mitte_Header_1920x560px.jpg
        type: image/jpeg
        size: 968096
        path: user/pages/04.standorte/03.mitte/Standorte_Mitte_Header_1920x560px.jpg
---

<a class="button button--program" href="/programm/mitte">Zum Programm <img src="{{ theme_url }}/images/icons/OdW_Icon_ZumProgramm_30x30px.svg" alt="Programm Mitte" /></a>
<div class="box">
<h2>Herzlich willkommen in unserer Begegnungsstätte Mitte!</h2>
<p>Gegenüber der U-Bahn Burgstraße in einem modernen und barrierefreien Bürogebäude befindet sich unsere Begegnungsstätte Mitte. Wenn Sie Fragen haben oder sich genauer über unsere Angebote informieren wollen, stehen wir Ihnen als Ansprechpartner für den Bezirk Hamburg-Mitte zur Verfügung.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['Standorte_Mitte_Content_1_810x400px.jpg'].html('','Teamleiterin Anke Bamberger') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Unsere Bezirksleiterin Anke Bamberger</h2>
	<p>und ihr Team werden Sie gern über Op de Wisch  informieren, Ihnen die unterschiedlichen Hilfsangebote vorstellen und Sie im Antragsverfahren unterstützen und begleiten, wenn Sie eine Hilfe in Anspruch nehmen wollen. </p>
    <p>An unserem Standort Mitte können wir mit unserem multikulturellen Team Beratungsgespräche und eine Bezugsbetreuung in vielen Sprachen wie z.B. Farsi, Dari, Arabisch, Englisch und Twi (Ghana) anbieten, auf Wunsch auch anonym. Im Rahmen unserer offenen Angebote bieten wir u.a. die Sozial- und Migrationsberatung jeden ersten Donnerstag im Monat in Dari/Farsi, jeden zweiten Donnerstag auf Twi/Englisch sowie jeden dritten Donnerstag auf Arabisch an - jeweils in der Zeit von 12-14 Uhr.</p>
    <p>Ein weiterer Schwerpunkt unserer Arbeit ist die intensive Zusammenarbeit mit GenesungsbegleiterInnen, die aus Erfahrung vertraut sind mit seelischen Krisen und der Möglichkeit, diese zu bewältigen. Als ausgebildete BeraterInnen bieten diese die Genesungsbegleiter-Sprechstunde an.</p>
    <p>Auch für die Angehörigen von psychisch erkrankten Menschen können wir Unterstützung anbieten. Unsere Angehörigenbegleiterin Frau Seiler-Lübbecke steht allen Interessierten unbürokratisch mit viel Erfahrung, Fachwissen und offenem Ohr zur Seite.</p>
    <p>Im Rahmen von Resilienzgruppen (nur für KlientInnen) besteht die Möglichkeit, gemeinsam mit anderen Betroffenen die eigenen Stärken und Ressourcen wieder zu entdecken oder Neue zu entwickeln.</p>
   
	<p>Wir freuen uns über alle Besucher und vereinbaren gerne mit Ihnen ein unverbindliches Informationsgespräch.</p>
    </div>
</div>
<hr>
<div class="grid box">
    <div class="col-6 left col-sm-12">
    <h2>So erreichen Sie uns</h2>
	<p>Op de Wisch e.V.<br>
	Grootsruhe 2 (ggü. U-Bahn Burgstraße)<br>
	20537 Hamburg<br>
    </p>
    <p>Telefon <a href="tel:+4940883067670">040_88 30 67 67 0</a><br>
    Telefax 040_88 30 67 67 9<br>
    <a href="mailto:info@op-de-wisch.de">info@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
	<p>Anke Bamberger<br>
    Bezirksleitung Mitte<br>
    Mobil <a href="tel:+491704520510">0170_452 05 10</a><br>
	<a href="mailto:bamberger@op-de-wisch.de">bamberger@op-de-wisch.de</a></p> {# TO-BE-SECURED #}

  <a class="button button--bus" href="http://www.hvv.de/fp.php?id=739c9ff47a90a584df1406648275a212" target="_blank">{{ page.media['hvv_logo.svg'].html('','Mit dem HVV zu uns') }}</a>
    </div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['Standorte_Mitte_Content_3_810x400px.jpg'].html('','Begegnungsstaette Mitte') }}
        {{ page.media['Standorte_Mitte_Content_4_810x400px.jpg'].html('','Begegnungsstaette Mitte') }}
    </div>
</div>
<hr>
<div class="box">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2370.152406418202!2d10.040213216201424!3d53.55504678002333!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b18ebd9b9004df%3A0xa385988105a72451!2sOp+de+Wisch+e.V.!5e0!3m2!1sen!2sde!4v1495795728710" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
