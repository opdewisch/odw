---
title: Wandsbek
metadata:
    description: 'Gabi Schierstedt und ihr Team stehen als Ansprechpartner für den Bezirk Wandsbek zur Verfügung. '
    keywords: 'Ambulante Sozialpsychiatrie, Wandsbek, Gabi Schierstedt, Rüterstraße 71'
wrapperBackground: true
headerImageAlt: 'Begegnungsstätte Wandsbek'
pageColor: orange
headerImage:
    user/pages/04.standorte/05.wandsbek/Standorte_Wandsbek_Header_1920x560px.jpg:
        name: Standorte_Wandsbek_Header_1920x560px.jpg
        type: image/jpeg
        size: 933951
        path: user/pages/04.standorte/05.wandsbek/Standorte_Wandsbek_Header_1920x560px.jpg
---

<a class="button button--program" href="/programm/wandsbek">Zum Programm <img src="{{ theme_url }}/images/icons/OdW_Icon_ZumProgramm_30x30px.svg" alt="Programm Wandsbek" /></a>
<div class="box">
<h2>Herzlich willkommen in unserer Begegnungsstätte Wandsbek!</h2>
<p>Zentral und gut erreichbar, aber verkehrsberuhigt im Innenhof befindet sich unsere Begegnungsstätte Wandsbek. Wenn Sie Fragen haben oder sich genauer über unsere Angebote informieren wollen, stehen wir Ihnen als Ansprechpartner für den Bezirk Hamburg-Wandsbek zur Verfügung.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['Standorte_Wandsbek_Content_1_810x400px.jpg'].html('','Teamleiterin Gabi Schierstedt') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Unsere Bezirksleiterin Gabi Schierstedt</h2>
	<p>und ihr Team werden Sie gern über Op de Wisch  informieren, Ihnen die unterschiedlichen Hilfsangebote vorstellen und Sie im Antragsverfahren unterstützen und begleiten, wenn Sie eine Hilfe in Anspruch nehmen wollen.</p>
    <p>Wir freuen uns über alle Besucher und vereinbaren gerne mit Ihnen ein unverbindliches Informationsgespräch!</p>
	<p>Wir kooperieren im Stadtteil mit Gosa gGmbH (Trommelgruppe).</p>
    </div>
</div>
<hr>
<div class="grid box">
    <div class="col-6 left col-sm-12">
    <h2>So erreichen Sie uns</h2>
	<p>Op de Wisch e.V.<br>
	Rüterstraße 71 (Innenhof)<br>
	22041 Hamburg<br>
    </p>
    <p>Telefon <a href="tel:+4940675871370">040_67 58 71 37 0</a><br>
    Telefax 040_67 58 71 37 9<br>
    <a href="mailto:info@op-de-wisch.de">info@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
	<p>Gabi Schierstedt<br>
    Bezirksleitung Wandsbek<br>
	Mobil 0176_348 37 964<br>
	<a href="mailto:schierstedt@op-de-wisch.de">schierstedt@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
    <p>Öffnungszeiten:<br>
    Montag und Donnerstag verbindlich von 10:00 bis 18:00 Uhr.<br>
	An allen anderen Tagen je nach Angebot und Absprache. </p>

  <a class="button button--bus" href="http://www.hvv.de/fp.php?id=2182c0f466ae030cce40af340d70877f" target="_blank">{{ page.media['hvv_logo.svg'].html('','Mit dem HVV zu uns') }}</a>
    </div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['Standorte_Wandsbek_Content_3_810x400px.jpg'].html('','Begegnungsstaette Wandsbek') }}
        {{ page.media['Standorte_Wandsbek_Content_4_810x400px.jpg'].html('','Begegnungsstaette Wandsbek') }}
    </div>
</div>
<hr>
<div class="box">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2369.0525591635555!2d10.073382516000212!3d53.574678565284486!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b18bff39cf7671%3A0x8bdc777e6c4ff552!2sR%C3%BCterstra%C3%9Fe+71%2C+22041+Hamburg!5e0!3m2!1sde!2sde!4v1495795929722" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
