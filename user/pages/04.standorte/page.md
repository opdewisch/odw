---
title: Standorte
metadata:
    description: 'Verteilt über die zentralen Stadtteile Hamburgs befinden sich die Begegnungsstätten von Op de Wisch. Hier finden Gruppenangebote und Beratungen statt.'
    keywords: 'Begegnungsstätte, Programm, Begegnung, Hamburg, HH, Wandsbek, Nord, Eimsbüttel, Mitte, Duvenstedt, Altona, Sozialberatung, Angebotsberatung, Migrationsberatung'
headerImageAlt: 'Immer in Reichweite'
pageColor: orange
headerImage:
    user/pages/04.standorte/Standorte_Header_1920x560px.jpg:
        name: Standorte_Header_1920x560px.jpg
        type: image/jpeg
        size: 1187210
        path: user/pages/04.standorte/Standorte_Header_1920x560px.jpg
navigationIcon:
    user/pages/04.standorte/OdW_Icon_Standorte_170x125px_AKTIV.svg:
        name: OdW_Icon_Standorte_170x125px_AKTIV.svg
        type: image/svg+xml
        size: 2146
        path: user/pages/04.standorte/OdW_Icon_Standorte_170x125px_AKTIV.svg
content:
    items: '@self.modular'
    order:
        by: header.news_date
        dir: asc
---

## Willkommen in den Begegnungsstätten von Op de Wisch!
Verteilt über die zentralen Stadtteile Hamburgs und immer in Reichweite des öffentlichen Nahverkehrs befinden sich die Begegnungsstätten von Op de Wisch. Hier finden einerseits unsere wöchentlichen Gruppenangebote, Veranstaltungen, Beratungen und offenen Angebote statt und andererseits können Sie dort Ihre BezugsbetreuerInnen treffen oder die Sprechstunde der GenesungsbegleiterInnen besuchen.
<br>
<br>

{% for module in page.collection() %}
{{ module.content }}
{% endfor %}
