---
title: Nord
metadata:
    description: 'Anja Paul und ihr Team stehen als Ansprechpartner für den Bezirk Nord zur Verfügung. '
    keywords: 'Ambulante Sozialpsychiatrie, Nord, Anja Paul, Alter Teichweg 11'
wrapperBackground: true
headerImageAlt: 'Begegnungsstätte Nord'
pageColor: orange
headerImage:
    user/pages/04.standorte/04.nord/Standorte_Nord_Header_1920x560px.jpg:
        name: Standorte_Nord_Header_1920x560px.jpg
        type: image/jpeg
        size: 737507
        path: user/pages/04.standorte/04.nord/Standorte_Nord_Header_1920x560px.jpg
---

<a class="button button--program" href="/programm/nord">Zum Programm <img src="{{ theme_url }}/images/icons/OdW_Icon_ZumProgramm_30x30px.svg" alt="Programm Nord" /></a>
<div class="box">
<h2>Herzlich willkommen in unserer Begegnungsstätte Nord!</h2>
<p>Sehr zentral zwischen den U-Bahnstationen Hamburger Straße (ca. 3 Minuten fußläufig) und Dehnhaide (ca. 7 Minuten fußläufig) befindet sich unsere Begegnungsstätte Nord. Wir sitzen im Hinterhaus im 1. Stock. Wenn Sie Fragen haben oder sich genauer über unsere Angebote informieren wollen, stehen wir Ihnen als Ansprechpartner für den Bezirk Hamburg-Nord zur Verfügung.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['Standorte_Nord_Content_1_810x400px.jpg'].html('','Teamleiterin Anja Paul') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Unsere Bezirksleiterin Anja Paul</h2>
	<p>und ihr Team werden Sie gern über Op de Wisch  informieren, Ihnen die unterschiedlichen Hilfsangebote vorstellen und Sie im Antragsverfahren unterstützen und begleiten, wenn Sie eine Hilfe in Anspruch nehmen wollen.</p>
	<p>Wir freuen uns über alle Besucher und vereinbaren gerne mit Ihnen ein unverbindliches Informationsgespräch!</p>
	<p>Wir kooperieren im Stadtteil mit ABeSa, FIGA Wohnverbund e.V., GPD Hamburg Nordost GmbH, Hamburgische Brücke, Insel e.V., Projekt März und VIA e.V. Bitte erfragen Sie bei uns in der Begegnungsstätte, welche konkreten Angebote sie dort wahrnehmen können.</p>
    </div>
</div>
<hr>
<div class="grid box">
    <div class="col-6 left col-sm-12">
    <h2>So erreichen Sie uns</h2>
	<p>Op de Wisch e.V.<br>
    Marschnerstraße 9<br>
    22081 Hamburg<br>
    </p>
    <p>Telefon <a href="tel:+494021908163">040_21 90 81 63</a><br>
    Telefax 040_21 90 81 64<br>
    <a href="mailto:info@op-de-wisch.de">info@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
	<p>Anja Paul<br>
    Bezirksleitung Nord<br>
    Mobil <a href="tel:++4917657589693">0176_575 89 693</a><br>
	<a href="mailto:paul@op-de-wisch.de">paul@op-de-wisch.de</a></p> {# TO-BE-SECURED #}

  <a class="button button--bus" href="http://www.hvv.de/fp.php?id=a9ccce83b2f66d62066bfa2771768b3a" target="_blank">{{ page.media['hvv_logo.svg'].html('','Mit dem HVV zu uns') }}</a>
    </div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['Standorte_Nord_Content_3_810x400px.jpg'].html('','Begegnungsstaette Nord') }}
        {{ page.media['Standorte_Nord_Content_4_810x400px.jpg'].html('','Begegnungsstaette Nord') }}
    </div>
</div>
<hr>
<div class="box">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2368.9768711624183!2d10.035918451744744!3d53.57602937992982!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b18ecab161f4f5%3A0xbbb4e898fd0ede49!2sMarschnerstra%C3%9Fe+5-9%2C+22081+Hamburg!5e0!3m2!1sde!2sde!4v1516883205172" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
