---
title: Kontakt
metadata:
    description: 'Hier finden sich die besten Möglichkeiten, mit Op de Wisch in Kontakt zu treten. '
    keywords: 'Ute Peters, Jörg Zart, Zentrale, Verwaltung, Hamburg, HH, Gesa Dilling, Auf der Wiese'
footerPage: true
headerImageAlt: 'Verwaltung Op de Wisch im Erdgeschoss Grindelhochhäuser, Oberstraße 14b, Hamburg'
pageColor: grey
headerImage:
    user/pages/10.kontakt/Kontakt_Header_1920x560px.jpg:
        name: Kontakt_Header_1920x560px.jpg
        type: image/jpeg
        size: 1483986
        path: user/pages/10.kontakt/Kontakt_Header_1920x560px.jpg
---

## Nehmen Sie mit uns Kontakt auf!
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['Kontakt_Content_1_810x400px.jpg'].html('','Geschaeftsfuehrung und Gesamtleitung Ute Peters') }}
        {{ page.media['Kontakt_Content_2_810x400px.jpg'].html('','Stellvertretende Gesamtleitung und pädagogische Beratung Jörg Zart') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Mit der Zentrale:</h2>
	<p>Op de Wisch e.V.<br>
    Oberstraße 14b<br>
	20144 Hamburg</p>
    <p><a href="tel:+4940600883400">Telefon 040_600 88 34 00</a><br>
	Telefax 040_600 88 34 22<br>
    info@op-de-wisch.de</p>
    <p><strong>Ute Peters</strong>,<br>
    Gesamtleitung</p>
	<p><strong>Jörg Zart</strong>,<br>
	Stellvertretende Gesamtleitung,<br>
	pädagogische Beratung</p>
    </div>
</div>
<hr class="spacer spacer--bottom">
## Mit unseren Begegnungsstätten und Projekten:
[Altona](/standorte/altona){.link-internal}<br>
[Eimsbüttel](/standorte/eimsbuettel){.link-internal}<br>
[Mitte](/standorte/mitte){.link-internal}<br>
[Nord](/standorte/nord){.link-internal}<br>
[Wandsbek](/standorte/wandsbek){.link-internal}<br>
[Kinder-, Jugend- und Familienhilfe](/unser-angebot/kinder-jugend-und-familienhilfe){.link-internal}<br>
[Reit- und Naturprojekt](/projekte/reit-und-naturprojekt){.link-internal}<br>
<br>
<hr class="spacer spacer--top spacer--bottom">
## Mit unseren Klientensprechern:
Klientensprecher<br>
Telefon <a href="tel:+4917614475942">0176_14475942</a><br>
klientensprecher@op-de-wisch.de
<br>
