---
title: 'Autorenlesung am 30. Mai 2018!'
image_align: left
news_date: '2018-04-24'
---

Wir freuen uns sehr auf Joey Lamprecht, der am 30. Mai 2018 in der Begegnungsstätte Eimsbüttel aus seinem Buch "Meine letzte Psychose oder: Wie ich Gott und den Teufel traf" liest! Die Lesung beginnt um 18:30 Uhr in der Oberstraße 14b. Informationen über den Autoren und das Buch folgen [hier](Autorenlesung.pdf).