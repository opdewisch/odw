---
title: 'Sozialberatung in Dari/Farsi und Arabisch'
image_align: left
news_date: '2018-06-11'
---

Ab sofort bieten wir am Standort Mitte in der Grootsruhe 2 unsere Sozialberatung in Dari/Farsi sowie in Arabisch an. Jeden 1. Donnerstag im Monat von 12:00 bis 14:00 Uhr in Dari/Farsi und jeden 3. Donnerstag zur gleichen Zeit in Arabisch. Alle Infos dazu finden sich unter Programm und Standort Mitte auf dieser Homepage. 