---
title: 'Endlich mehr Platz am Standort Altona'
image_align: left
news_date: '2018-07-27'
---

Unsere Begegnungsstätte Altona erweitert ihre Räume und bietet jetzt auch zusätzlich zur Eimsbütteler Str. 95 in der Kieler Str. 60 einen Ort der Begegnung an. Wir haben dadurch endlich mehr Platz für unsere Angebote, insbesondere für mehr Sozialberatung. Am 16.08.2018 ab 15 Uhr wollen wir allen Interessierten die neuen Räume vorstellen. Weitere Informationen gibt es [hier](einladung%20altona.pdf). Wir freuen uns auf Sie!