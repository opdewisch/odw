---
title: '"Come Together" am 29. August 2018'
image_align: left
news_date: '2018-08-18'
---

Am Mittwoch, den 29. August 2018 wird gefeiert! Die Kooperationspartner-Nord der ASP laden mittlerweile das dritte Jahr infolge zum "Come Together"! Alle genaueren Informationen gibt es [hier](Einladung_Come_Together.pdf). Welche Träger gemeinsam feiern gibt es [hier](Einladung_Come_Together_Koop.pdf) zu sehen.  