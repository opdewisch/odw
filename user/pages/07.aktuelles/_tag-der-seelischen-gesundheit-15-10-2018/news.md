---
title: 'Tage der seelischen Gesundheit '
image_align: left
news_date: '2018-09-03'
---

Im Oktober engagieren wir uns auf mehreren Veranstaltungen jeweils zum "Tag der seelischen Gesundheit". 
Am Montag, den 15. Oktober 2018 sind wir von 15:00 bis 19:00 Uhr im Hamburg-Haus im Doormannsweg 12. Wir haben einen eigenen Stand mit Informationen sowie eine eigene Präsenz unserer Malgruppe. Zusätzlich freuen wir uns auf 2 stimmgewaltige Auftritte unserer Chores. Alle Infos dazu gibt es [hier](https://www.hamburg.de/contentblob/11411330/c70894ef023085973da48d84539bbb96/data/flyer-singen-swingen-schwingen2018.pdf). 

Am Mittwoch, den 17. Oktober sind wir von 10:00 bis 20:00 Uhr im Mercado. Auch dort informieren wir rund um die Themen Ambulante Sozialpsychiatrie und Kinder-, Jugend- und Familienhilfe. 
Wir freuen uns auf viele BesucherInnen!