---
title: 'Aktiv am Dienstag - Garten und Handwerk im Reitprojekt mit Frühstück!'
image_align: left
news_date: '2018-05-14'
---

Ab sofort wollen wir wieder jeden Dienstag auf unserer Reitanlage in Hamburg-Duvenstedt aktiv werden. Wir treffen uns um 10:00 Uhr und arbeiten ca. eine Stunde. Nach getaner Arbeit gibt es dann ab 11:00 Uhr ein gemeinsames Frühstück. Alle weiteren Informationen finden Sie [hier](Aktiver%20Dienstag.pdf). 