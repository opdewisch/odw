---
title: 'Der Gründervater von Op de Wisch ist gestorben.'
image_align: right
published: true
cache_enable: false
news_date: '2017-06-23'
images:
    user/pages/07.aktuelles/_der-gruendervater-von-op-de-wisch-ist-gestorben/arnold_boecker-op-de-wisch.jpg:
        name: arnold_boecker-op-de-wisch.jpg
        type: image/jpeg
        size: 285677
        path: user/pages/07.aktuelles/_der-gruendervater-von-op-de-wisch-ist-gestorben/arnold_boecker-op-de-wisch.jpg
---

Nach kurzer schwerer Krankheit ist Arnold Böcker am vergangenen Freitag gestorben. In mehr als 20 Jahren hat er maßgeblich den Verein Op de Wisch aufgebaut und geprägt. Mit viel Herz, Engagement und durchaus streitbar hat er KlientInnen auf dem Weg in ein selbstbestimmteres Leben begleitet und unterstützt. Auch MitarbeiterInnen hat er immer ermahnt, im Engagement für dieses Ziel nicht nachzulassen. Seit 2013 war er im Ruhestand, hat aber weiterhin Kontakt gehalten und gepflegt. Wir freuen uns, ihn noch am 17. Juni bei unserem afrikanischen Sommerfest im Reitprojekt in guter Stimmung erlebt zu haben. So wollen wir ihn in Erinnerung behalten und dankbar auf sein Leben und Wirken zurückschauen.