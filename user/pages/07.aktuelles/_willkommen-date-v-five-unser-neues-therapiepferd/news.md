---
title: 'Willkommen, Date V (Five) - Unser neues Therapiepferd!'
media_order: Therapiepferd_DateV.jpeg
image_align: right
news_date: '2019-06-17'
images:
    user/pages/07.aktuelles/_willkommen-date-v-five-unser-neues-therapiepferd/Therapiepferd_DateV.jpeg:
        name: Therapiepferd_DateV.jpeg
        type: image/jpeg
        size: 148409
        path: user/pages/07.aktuelles/_willkommen-date-v-five-unser-neues-therapiepferd/Therapiepferd_DateV.jpeg
---

Nach langer und intensiver Suche haben wir nun endlich einen neuen Kollegen: 
„Date V“ verstärkt ab sofort unser Pferdeteam, was für beide Seiten spannend und mit ein bisschen Arbeit verbunden ist. Seinen schönen Namen hat er in Anlehnung an den edler Spender erhalten und wir werden nicht müde, der Datev eG dafür herzlich zu danken. <p>
An ein Therapiepferd werden besondere Anforderungen gestellt und entschieden haben wir uns für ein 7-jähriges sächsisches Warmblut. Diese Rasse gilt als ausdauernd, lernfähig und zuverlässig und entspricht damit exakt unserem Bedarf. Date V zeichnet sich zusätzlich durch sein menschenbezogenes, freundliches Gemüt aus und verteilt gerne an den einen oder anderen Besucher eine persönliche Begrüßung. Nach der aufregenden Fahrt im Hänger ins neue Zuhause, einem gründlichem Tierarzt-Check Up, einem Zahnarztbesuch, einer Physiotherapie-Einheit und dem Besuch vom Sattler sind Ausrüstung und Pferd nun startklar. Date V darf jetzt seinen Platz in der kleinen Herde finden und nach ersten Trainingsstunden die nähere Umgebung erkunden. Wir freuen uns, dass er bei uns ist!



