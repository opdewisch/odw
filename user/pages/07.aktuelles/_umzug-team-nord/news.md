---
title: 'Umzug Team Nord'
image_align: left
news_date: '2017-12-18'
---

Endlich haben wir für unsere Begegnungsstätte Nord schöne neue Räume gefunden, in die wir im Januar 2018 einziehen werden! Während des Umzugs können wir nur ein eingeschränktes Programm anbieten. Alle wichtigen Informationen zu unserem Umzug und allen aktuellen Änderungen gibt es [hier](Team%20Nord%20zieht%20um.pdf).