---
title: 'Bundesfreiwilligendienst - freie Stelle zu vergeben'
media_order: 'BFD Ausschreibung.pdf'
image_align: left
news_date: '2019-06-27'
---

Wir suchen für unseren Träger Unterstützung im Rahmen des Bundesfreiwilligendienstes. Alle weiteren Infos dazu finden sich  [hier](BFD%20Ausschreibung.pdf).