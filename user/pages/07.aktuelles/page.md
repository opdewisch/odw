---
title: Aktuelles
metadata:
    description: 'Hier findet sich alles, was gegenwärtig stattfindet, wie z.B. Ausflüge, Neuigkeiten aus dem Vereinsleben und Veranstaltungen. '
    keywords: 'Ausflug, Veranstaltung, Neues, Neuigkeiten, Hamburg, HH'
headerImageAlt: 'Abwechslungsreiches Programm'
pageColor: yellow
headerImage:
    user/pages/07.aktuelles/Aktuelles_Header_1920x560px.jpg:
        name: Aktuelles_Header_1920x560px.jpg
        type: image/jpeg
        size: 921331
        path: user/pages/07.aktuelles/Aktuelles_Header_1920x560px.jpg
navigationIcon:
    user/pages/07.aktuelles/OdW_Icon_Aktuelles_170x125px_AKTIV.svg:
        name: OdW_Icon_Aktuelles_170x125px_AKTIV.svg
        type: image/svg+xml
        size: 4096
        path: user/pages/07.aktuelles/OdW_Icon_Aktuelles_170x125px_AKTIV.svg
content:
    items: '@self.modular'
---

<div class="box">
  <h1>Was sich so tut bei Op de Wisch</h1>
  <p>Termine für Ausflüge und Veranstaltungen, Veröffentlichungen aus der Presse oder was wir sonst aktuell erleben geben wir hier an dieser Stelle bekannt.</p>
</div>
<div class="news">
{% set eventsRaw = page.children() %}
{% set events = eventsRaw.order('header.news_date', 'desc', null, 1) %}
{% for p in events %}
  <hr>
  {% if p.header.images %}
  <div class="grid">
  <div class="news-article{{ (p.header.image_align == 'left') ? ' right' : ' left' }} col-6 col-sm-12">
  {% else %}
  <div class="news-article">
  {% endif %}
    <h2>{{ p.title }}</h2>
    {{ p.content }}
    <span class="dates">{{ p.header.news_date|date("d.m.Y") }}</span>
  </div>
  {% for i in p.header.images %}
  <div class="col-6 col-sm-12 {{ p.header.image_align }}">
    {{ p.media[i.name].resize(400,400).quality(100).html }}
  </div>
  {% endfor %}
  {% if p.header.images %}
  </div>
  {% endif %}
{% endfor %}
</div>
