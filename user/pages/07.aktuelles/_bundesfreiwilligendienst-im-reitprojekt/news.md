---
title: 'Bundesfreiwilligendienst im Reitprojekt Hamburg-Duvenstedt'
image_align: left
news_date: '2018-08-20'
---

Wir suchen für unser eigenes Reitprojekt Unterstützung im Rahmen des Bundesfreiwilligendienstes. Alle weiteren Infos dazu finden sich [hier](BFD%20Ausschreibung%20Reitprojekt%20Duvenstedt.pdf).