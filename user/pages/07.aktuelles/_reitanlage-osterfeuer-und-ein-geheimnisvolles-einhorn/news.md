---
title: 'Theater auf der Reitanlage!'
image_align: left
news_date: '2018-05-13'
---

Auf unserer Reitanlage arbeitet das Op de Wisch-Reit-Team das ganze Jahr engagiert und konzentriert mit Kindern und Erwachsenen. Besondere Veranstaltungen haben aber auch ihren festen Platz und zu einem besonderen Highlight möchten wir hiermit herzlich einladen: Am Samstag, den 16. Juni 2018, wird es magisch mit "Harry Potter und das geheimnisvolle Einhorn"! Um Anmeldung wird gebeten; alle wichtigen Informationen und Kontaktdaten finden sich [hier](Reitprojekt_2018_Theater.pdf). 