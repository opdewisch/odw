---
title: 'Flohmarkt 16. Juni 2019 in der Marschnerstraße'
image_align: left
news_date: '2019-06-11'
---

Am Sonntag 16. Juni 2019 veranstalten wir einen Flohmarkt in der Zeit von 12:00 bis 16:00 Uhr. Wo? Op de Wisch Begegnungsstätte Nord in der Marschnerstraße 9. Grillgut, Getränke und Kuchen gibt’s zum kleinen Preis und vielleicht den ein oder anderen kleinen Schatz vom Flohmarktstand. Alle InteressentInnen sind herzlich willkommen! 