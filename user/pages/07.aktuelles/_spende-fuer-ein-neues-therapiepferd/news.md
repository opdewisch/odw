---
title: 'Spende für ein neues Therapiepferd'
image_align: right
news_date: '2019-01-29'
images:
    user/pages/07.aktuelles/_spende-fuer-ein-neues-therapiepferd/Datev.jpg:
        name: Datev.jpg
        type: image/jpeg
        size: 76050
        path: user/pages/07.aktuelles/_spende-fuer-ein-neues-therapiepferd/Datev.jpg
---

Wir sind der einzige ASP-Träger in Hamburg, der ein eigenes Reit- und Naturprojekt betreibt und das aus gutem Grund: Die Arbeit mit dem Pferd in der Natur ist eine große Bereicherung für unsere Klient*innen auf ihrem eigenen Weg. Die gesundheitsfördernde Wirkung von Tier und Natur wird bei uns als pädagogische Methode  genutzt, um kleine und große Menschen wieder ins Gleichgewicht und einen Schritt weiter zu bringen. Auch im Sozialraum ist das Reitprojekt unter der Leitung von Gudrun Sailer-Maaß offen für Kinder und Kindergruppen, die in einem geschützten Rahmen Kontakt zum Pferd aufbauen möchten. 

Unsere Therapiepferde zeichnen sich durch absolute Nervenstärke und ein liebes Wesen aus und bieten damit die ideale Voraussetzung für einen inspirierenden Umgang. Im vergangenen Jahr ist eines unserer Therapiepferde gestorben, so dass wir dringend ein neues, gut ausgebildetes Pferd erwerben wollen. Das Reitprojekt ist eine finanzielle Herausforderung und somit freuen wir uns immer wieder über großzügige Spender, die uns dieses besondere Angebot ermöglichen. 

Aktuell möchten wir uns von ganzem Herzen bei der DATEV bedanken, die uns mit der Summe von unglaublichen € 10.000 unterstützen, um ein neues Therapiepferd anschaffen zu können! DATEV verzichtet seit 1990 auf Weihnachtspräsente an Kunden und Geschäftspartner und spendet dieses Geld vielen unterschiedlichen sozialen Einrichtungen. Was für eine wunderbare Tradition! Der Kontakt zu DATEV wurde hergestellt durch den Geschäftsführer der Steuerberatungsgesellschaft WAGRIA in Lübeck, Herrn Daniel Reich, dessen Tochter bei Op de Wisch reitpädagogische Angebote in Anspruch nimmt. Unser Dank gilt deshalb auch ganz explizit der gesamten Familie Reich!
Das Foto zeigt Gudrun Sailer-Maaß (2. von links) und Daniel Reich (2. von rechts) bei der Übergabe der Geldspende durch die DATEV. 