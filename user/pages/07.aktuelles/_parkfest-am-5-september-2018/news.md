---
title: 'Parkfest am 5. September 2018'
image_align: left
news_date: '2018-08-18'
---

Die Psychosoziale-Arbeitsgemeinschaft Wandsbek lädt ein zum diesjährigen Parkfest! Am Mittwoch, den 5. September 2018 feiern wir von 12:00 bis 15:30 Uhr im Eichtalpark. Alle weiteren Informationen finden sich [hier](Einladung_Parkfest.pdf).