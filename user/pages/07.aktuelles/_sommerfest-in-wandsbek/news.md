---
title: 'Sommerfest in Wandsbek'
image_align: left
news_date: '2018-06-11'
---

Am Dienstag, den 26. Juni 2018 von 15:00 bis 18:00 Uhr feiern wir unser fünftes Sommerfest in der Begegnungsstätte Wandsbek! Alle KlientInnen sowie deren Kinder und Angehörige und natürlich unsere Nachbarn und Anwohner sind herzlich eingeladen in die Rüterstraße 71. Es wird je nach Wetter getrommelt, gesungen, gespielt und vieles mehr. Für den kleinen und großen Hunger ist gesorgt und Getränke sind natürlich ebenfalls vorhanden. Wir freuen uns auf ein buntes Sommerfest!