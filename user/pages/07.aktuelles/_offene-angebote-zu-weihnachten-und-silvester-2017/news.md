---
title: 'Offene Angebote zu Weihnachten und Silvester 2017'
image_align: left
itemsPerRow: '3'
mobileItemsPerRow: '2'
---

Der Vorstand der Genesungsbegleitung und Peerbegleitung Hamburg e.V. hat eine Liste zusammengestellt, die diverse offene Angebote zu Weihnachten und Silvester der ASP-Träger in Hamburg zusammenfasst. Alle genaueren Infos dazu finden sich hier. [](Offene Angebote zu Weihnachten und Silvester 2017.pdf)