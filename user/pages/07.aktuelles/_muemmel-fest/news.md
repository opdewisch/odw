---
title: 'Fest der seelischen Gesundheit am 9. November 2018'
image_align: left
news_date: '2018-10-01'
---

Am 9. November 2018 findet in Mümmelmannsberg von 14:00 bis 18:00 Uhr das Fest der seelischen Gesundheit statt. Alle weiteren Informationen finden sich [hier](Fest_der_seelischen_Gesundheit.pdf). 