---
title: 'Kinder-, Jugend- und Familienhilfe'
metadata:
    description: 'Für Kinder und Jugendliche unter 21 Jahren steht ein eigenes multiprofessionelles und multikulturelles Team zur Verfügung. Wir unterstützen Kinder, Jugendliche und Familien in unterschiedlichen Problemlagen und bieten sozialpädagogische Familienhilfe sowie Erziehungsbeistandschaften an. '
    keywords: 'Hamburg, HH, Erziehungsbeistandschaft, sozialpädagogische Familienhilfe, traumasensibel, psychische Belastung oder Erkrankung der Eltern, Gesa Dilling, Jugendhilfe, Kinderhilfe, Familienhilfe, interkulturelle Hilfe, Migration, tiergestütze Pädagogik, heilpädagogisches Reiten, Pferde, Hunde, Hilfe, Seelennot'
wrapperBackground: true
headerImageAlt: Geborgenheit
pageColor: purple
headerImage:
    user/pages/03.unser-angebot/kinder-jugend-und-familienhilfe/UnserAngebot_KiJuFaHilfe_Header_1920x560px.jpg:
        name: UnserAngebot_KiJuFaHilfe_Header_1920x560px.jpg
        type: image/jpeg
        size: 814708
        path: user/pages/03.unser-angebot/kinder-jugend-und-familienhilfe/UnserAngebot_KiJuFaHilfe_Header_1920x560px.jpg
---

<div class="box">
	<h2>Unterstützung für Familien</h2>
	<p>Für Kinder, Jugendliche und ihre Familien steht unter der Leitung von Gesa Dilling ein eigenes multiprofessionelles und multikulturelles Team zur Verfügung. Mit unserer jahrzehntelangen Erfahrung sowohl in der Sozialpsychiatrie als auch in der Kinder- und Jugendhilfe kennen wir unterschiedliche Problemlagen und die Bedürfnisse der beteiligten Menschen. Als konkrete Maßnahmen bieten wir Erziehungsbeistandschaften und sozialpädagogische Familienhilfe an. Gerne stehen wir Ihnen für ein Gespräch und ein Kennenlernen zur Verfügung, auf Wunsch natürlich auch anonym.</p>
    <p>Es ist uns ein besonderes Anliegen mit den Familien auf Augenhöhe zu arbeiten und den Menschen in seiner Individualität und mit seinen besonderen Stärken wahrzunehmen. Ein wertschätzender Blick von außen auf die gesamte Familie kann helfen, gemeinsam gute Lösungen zu finden. Dabei verfolgen wir einen systemischen und ganzheitlichen Ansatz und bieten „Hilfen aus einer Hand“.</p>
  <p>Wir sind Gründungsmitglieder und Kooperationspartner von <a href="http://www.seelennot-ev.de/cms/website.php" target="_blank">SeelenNot</a>. Arbeitsgemeinschaft zur Unterstützung von Familien mit seelisch kranken Eltern.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['UnserAngebot_KiJuFaHilfe_Content_1_810x400px.jpg'].html('','Hilfe für Kinder') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Psychische Belastung von Eltern und Kindern</h2>
		<p>Wir unterstützen Familien bzw. Kinder in Familien, in denen Familienmitglieder psychisch erkrankt oder belastet sind. Ein wichtiges Ziel bei der Arbeit mit belasteten Familien ist vor allem, Kinder und Jugendliche emotional und sozial zu stabilisieren. Dabei arbeiten unsere Mitarbeiter traumasensibel.</p>
    </div>
</div>
<hr>
<div class="grid box">
    <div class="col-6 left col-sm-12">
    <h2>Interkulturelle Hilfen</h2>
		<p>Auch in den interkulturellen Hilfen sind viele unserer Mitarbeiter aktiv, die oft selber Migrationshintergrund besitzen. Sie kennen die Probleme von Migrationsfamilien, in denen die Integration häufig vorrangig durch die Kinder stattfindet, was die Familien vor eine große Herausforderung stellt. Brücken bauen, auch innerhalb von Familien, ist hier häufig eine der Hauptaufgaben für die Mitarbeiter.</p>
    </div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['UnserAngebot_KiJuFaHilfe_Content_2_810x400px.jpg'].html('','Interkulturelle Hilfen') }}
    </div>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['UnserAngebot_KiJuFaHilfe_Content_3_810x400px.jpg'].html('','Tiergestützte Pädagogik') }}
    </div>
	<div class="col-6 right col-sm-12">
    <h2>Tiergestützte Pädagogik</h2>
		<p>Ob als Vermittler, Türöffner oder Verstärker – immer wieder helfen uns die Tiere bei der Arbeit mit Kindern, Jugendlichen und ihren Familien. Auf unserer Reitanlage haben wir eigene Pferde, Katzen und Hühner und bei Bedarf somit die Möglichkeit, heilpädagogisches Reiten anzubieten. Unsere Unterstützung durch die Vierbeiner wird abgerundet durch Hunde, wie z.B. unsere Collie-Hündin „Elli“, oft mit dabei in Familien, bei Sitzungen und im Büro.</p>
	</div>
</div>
<hr>
<div class="grid box">
    <div class="col-6 left col-sm-12">
    <h2>So erreichen Sie uns</h2>
	<p>Op de Wisch e.V.<br>
	Oberstraße 14b<br>
	20144 Hamburg<br>
    </p>
    <p>Telefon <a href="tel:+4940600883400">040_600 88 34 00</a><br>
    Telefax 040_600 88 34 22<br>
    <a href="mailto:info@op-de-wisch.de">info@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
	<p>Gesa Dilling<br>
    Bereichsleitung Kinder-, Jugend- und Familienhilfe<br>
    Mobil <a href="tel:+4917634825716">0176_348 257 16</a><br>
    <a href="mailto:dilling@op-de-wisch.de">dilling@op-de-wisch.de</a></p> {# TO-BE-SECURED #}
    </div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['UnserAngebot_KiJuFaHilfe_Content_4_810x400px.jpg'].html('','Gesa Dilling') }}
    </div>
</div>
