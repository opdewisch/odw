---
title: 'Ambulante Sozialpsychiatrie'
metadata:
    description: 'Im Rahmen der Eingliederungshilfe unterstützen wir Menschen ab 21 Jahren bei seelischer Erkrankung oder Belastung. Wir bieten Beratung, Begleitung und Begegnung an. '
    keywords: 'Hamburg, HH, ASP, Eingliederungshilfe, Lebenskrise, Beratung, Begleitung, Begegnung, Ambulante Sozialpsychiatrie, Wohnassistenz, pädagogische Begleitung im eigenen Wohnraum, persönliches Budget, Sozialberatung, Angebotsberatung, Beratung durch GenesungsbegleiterInnen, Beratung für Angehörige'
wrapperBackground: true
headerImageAlt: 'Hilfe, die Früchte trägt'
pageColor: purple
headerImage:
    user/pages/03.unser-angebot/ambulante-sozialpsychiatrie/UnserAngebot_ASP_Header_1920x560px.jpg:
        name: UnserAngebot_ASP_Header_1920x560px.jpg
        type: image/jpeg
        size: 666753
        path: user/pages/03.unser-angebot/ambulante-sozialpsychiatrie/UnserAngebot_ASP_Header_1920x560px.jpg
---

<div class="box">
<h2>Ambulante Sozialpsychiatrie und andere Maßnahmen der Eingliederungshilfe</h2>
<p>Wir unterstützen Sie, wenn Sie seelisch erkrankt oder belastet sind, sich in einer akuten Lebenskrise befinden oder aus anderen Gründen Hilfe bei der Alltagsgestaltung brauchen. Im Rahmen der Eingliederungshilfe sind wir für Sie da, wenn Sie 21 Jahre oder älter sind und <strong>Beratung</strong> <strong>Begleitung</strong> oder <strong>Begegnung</strong> suchen. Wir bieten Ihnen ein umfangreiches Beratungsangebot sowie diverse Einzel- und Gruppenaktivitäten. Sie sind uns immer willkommen und können auch ohne Antrag an Angeboten teilnehmen.</p>
<p>Bei Bedarf bieten wir Ihnen eine längerfristige und intensivere Unterstützung. Wir helfen Ihnen bei der Beantragung von Leistungen, wie z.B.</p>
<ul>
<li>Ambulante Sozialpsychiatrie</li>
<li>Pädagogische Betreuung im eigenen Wohnraum</li>
<li>Wohnassistenz</li>
</ul>
<p>Wir bieten Ihnen unsere Maßnahmen auch im Rahmen des persönlichen Budgets an.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['UnserAngebot_ASP_Content_1_810x400px.jpg'].html('','Helfende Haende') }}
        {{ page.media['UnserAngebot_ASP_Content_2_810x400px.jpg'].html('','Beratung') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Beraten</h2>
		<p>Kommen Sie erst einmal zu uns! Wir bieten Ihnen kostenfreie Beratungen zu verschiedenen Schwerpunkten an. Immer auf Augenhöhe, offen für jeden und ohne, dass Sie dafür einen Antrag stellen müssen.</p>
        <ul class="short">
        <li>Die <strong>Sozialberatung</strong> steht allen Menschen zur Verfügung, die sich Hilfe wünschen in finanziellen, behördlichen und schriftlichen Belangen. Außerdem bieten wir Unterstützung rund um die Themen Wohnen und Arbeit.</li>
        <li>Unsere <strong>Angebotsberatung</strong> richtet sich an Menschen in akuten Lebenskrisen und persönlichen wie seelischen Notlagen. Hier finden wir gemeinsam heraus, welche konkreten Maßnahmen es für Sie gibt und bieten Ihnen Unterstützung bei der Antragstellung.</li>
        <li><strong>Beratung durch ausgebildete GenesungsbegleiterInnen</strong>, die als „Experten aus Erfahrung“ die Teams bereichern. Auf diese Weise können wir differenziertere Angebote für Menschen in ähnlichen Krisensituationen, ein erweitertes Verständnis von psychischen Belastungen sowie ein vielfältiges Wissen über Genesung fördernde Faktoren anbieten.</li>
        <li><strong>Beratung für Angehörige</strong> durch eine ausgebildete Angehörigenbegleiterin, ebenfalls eine „Expertin aus Erfahrung“. Psychische Erkrankungen treffen das gesamte soziale Umfeld. Bei diesem Angebot stehen die Angehörigen im Mittelpunkt und erhalten Beistand in Krisen, einen Austausch über Lösungsmöglichkeiten, Unterstützung zur Besinnung auf eigene Stärken und Informationen über weitere Hilfesysteme.</li>
</ul>
    </div>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	<h2>Begleiten</h2>
			<p>Eine längerfristige und intensivere Zusammenarbeit übernimmt eine BezugsbetreuerIn, der oder die Sie in dieser Zeit persönlich begleitet. In welchem Umfang hängt vom individuellen Hilfeplan ab, den wir gemeinsam mit Ihnen erstellen und der Ihre Interessen, Bedürfnisse und Ziele einbezieht.
      Dabei liegt der Fokus auf Hilfen zur Selbsthilfe (auch Empowerment), um Ihre Lebenssituation zu stabilisieren und Ihre Selbstbestimmung wieder herzustellen.</p>
			<p>Natürlich begleiten wir Sie bei Bedarf auch zu Ärzten und Behörden oder suchen Sie zu Hause auf. Wir arbeiten auf Wunsch mit geschlechtsspezifischen BezugsbetreuerInnen und bieten Ihnen als KlientInnen ein Mitbestimmungsrecht bei der Auswahl.</p>
    	</div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['UnserAngebot_ASP_Content_3_810x400px.jpg'].html('','Begleitung') }}
    </div>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['UnserAngebot_ASP_Content_4_810x400px.jpg'].html('','Begegnung') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Begegnen</h2>
    <p>Unsere Begegnungsstätten bieten Raum für vielfältige Aktivitäten und Kontakte. Hier treffen sich Menschen in vergleichbaren Lebenssituationen und können gemeinsam herausfinden, was ihre Genesung zusätzlich fördert.</p>
    <p>Neben offenen Gruppen können Sie bei themenbezogenen Angeboten und Ausflügen mitmachen. Wir sind an 5 Standorten im Stadtgebiet Hamburg vertreten: Bezirk Nord, Wandsbek, Mitte, Eimsbüttel und Altona - und immer in HVV-Nähe.</p>
        <p>Darüber hinaus bieten wir Ihnen spezielle, psycho-sozial ausgerichtete Angebote in einem geschützten, vertraulichen Rahmen.</p>
        <p>Welche Gruppenangebote es im Einzelnen gibt, präsentieren Ihnen unsere fünf Standorte unter Programm.</p>
    </div>
</div>
