---
title: Verlinkungen
itemsPerRow: 2
mobileItemsPerRow: 1
---

<div class="teaserbox">
  <a class="purple" href="/unser-angebot/ambulante-sozialpsychiatrie">
    <div class="teaserbox-hl">Ambulante Sozialpsychiatrie</div>
    {{ page.media['UnserAngebot_Link_ASP_810x120px.jpg'].html('','Helfende Haende') }}
  </a>
</div>

---

<div class="teaserbox">
  <a class="purple" href="/unser-angebot/kinder-jugend-und-familienhilfe">
    <div class="teaserbox-hl">Kinder-, Jugend- und Familienhilfe</div>
    {{ page.media['UnserAngebot_Link_KiJuFaHilfe_810x120px.jpg'].html('','Hilfe fuer Kinder und Jugendliche') }}
  </a>
</div>