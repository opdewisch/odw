---
title: 'Unser Angebot'
media_order: 'OdW_Icon_UnserAngebot_170x125px_AKTIV.svg,UnserAngebot_foodsharing_450x300px.jpg,UnserAngebot_HamburgerTafel_450x300px.jpg,UnserAngebot_Hamburg_HVVZeitkarten_450x300px.jpg,UnserAngebot_Hamburg_kostenlos_450x300px.jpg,UnserAngebot_Hamburg_Sozialkaufhaeuser_450x300px.jpg,UnserAngebot_Header_1920x560px.jpg,UnserAngebot_KulturLebenHamburg_450x300px.jpg'
metadata:
    description: 'Unser Angebot richtet sich an alle Menschen, die aufgrund von seelischen Notlagen Unterstuetzung benötigen und umfasst Eingliederungshilfe/ambulante Sozialpsychiatrie sowie sozialpädagogische Familienhilfe und Erziehungsbeistandschaften. '
    keywords: 'Beratung, Begleitung, Begegnung, Ambulante Sozialpsychiatrie, Eingliederungshilfe, Kinder, Jugend, Familienhilfe, Wohnassistenz, persönliches Budget, pädagogische Begleitung im eigenen Wohnraum, Sozialberatung, Angebotsberatung, Genesungsbegleiter, Angehoerigenbegleitung, Empowerment, Inklusion, traumasensibel, Erziehungsbeistandschaften, sozialpädagogische Familienhilfe, tiergestütze Paedagogik, Migration, Gemeindepsychiatrie, aufsuchend, ambulant, Hamburg, HH, Gesa Dilling, psycho-soziale Projekte, psycho-soziale Begleitung'
headerImageAlt: 'Der Weg zu Op de Wisch'
pageColor: purple
headerImage:
    user/pages/03.unser-angebot/UnserAngebot_Header_1920x560px.jpg:
        name: UnserAngebot_Header_1920x560px.jpg
        type: image/jpeg
        size: 713199
        path: user/pages/03.unser-angebot/UnserAngebot_Header_1920x560px.jpg
navigationIcon:
    user/pages/03.unser-angebot/OdW_Icon_UnserAngebot_170x125px_AKTIV.svg:
        name: OdW_Icon_UnserAngebot_170x125px_AKTIV.svg
        type: image/svg+xml
        size: 3897
        path: user/pages/03.unser-angebot/OdW_Icon_UnserAngebot_170x125px_AKTIV.svg
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _links
            - _flyer
---

### Hilfe in schwierigen Lebenslagen – wir geben Ihnen Halt!

Unser Angebot richtet sich an alle Menschen, die Unterstützung in ihrer alltäglichen Lebensgestaltung oder aufgrund von Notlagen benötigen. Schwerpunktmäßig betreuen wir Menschen im Rahmen der ambulanten Sozialpsychiatrie, die seelisch erkrankt oder belastet sind. Darüber hinaus bieten wir auch andere Maßnahmen der Eingliederungshilfe an. Unsere Kinder-, Jugend- und Familienhilfe unterstützt Familien in unterschiedlichen Problemlagen.

Alle unsere Maßnahmen haben das Ziel, emotional zu stabilisieren und Ihre größtmögliche Selbstbestimmung zu erreichen. Wir unterstützen Sie je nach Bedarf in Ihrem Wohn- oder Sozialraum und in unseren Begegnungsstätten. Derzeit betreuen wir Menschen aus über 10 Nationen.
<br>
<br>

{{ page.collection() | first.content }}

<hr class="spacer spacer--top">
<div class="box">
    <h2>Unser Angebot ganz konkret – unsere Flyer:</h2>
    <p>Zu unseren einzelnen Angeboten haben wir Flyer und Einleger, die einen genaueren Überblick liefern. Einfach anklicken und bei Bedarf ausdrucken!</p>
    {{ page.collection() | last.content }}

</div>
<hr class="spacer spacer--top">
<div class="box">
    <h2>Nicht von uns - aber auch interessant:</h2>
	<p>Hier haben wir ergänzend einige Links zusammengestellt, die für Menschen mit geringem Einkommen und mit Unterstützungsbedarf in Hamburg hilfreich sind. Hamburg.de bietet kostenlose Tipps sowie verschiedene Vergünstigungen an, wie z.B. Sozialkaufhäuser und die HVV-Sozialkarte. </p>
	<p>Der Verein KulturLeben Hamburg bietet kostenlose Eintrittskarten für verschiedene Kulturangebote in Hamburg. Oxfam bietet Second Hand-Ware zum fairen Preis und unterstützt mit den Einnahmen wiederum diverse soziale Projekte auf der ganzen Welt. Die Hamburger Tafel bietet verschiedene kulinarische Hilfsangebote.</p>
    <span class="logos">
    <a href="http://www.hamburg.de/kostenlos" target="_blank">{{ page.media['UnserAngebot_Hamburg_kostenlos_450x300px.jpg'].html('','hamburg.de kostenlos') }}</a>
    <a href="http://www.hamburg.de/sozialkaufhaeuser" target="_blank">{{ page.media['UnserAngebot_Hamburg_Sozialkaufhaeuser_450x300px.jpg'].html('','hamburg.de Sozialkaufhäuser') }}</a>
    <a href="http://www.hamburg.de/sozialkarte" target="_blank">{{ page.media['UnserAngebot_Hamburg_HVVZeitkarten_450x300px.jpg'].html('','hamburg.de HVV-Zeitkarte') }}</a>
    </span>
    <span class="logos">
    <a href="http://www.hamburger-tafel.de/hilfseinrichtungen.html" target="_blank">{{ page.media['UnserAngebot_HamburgerTafel_450x300px.jpg'].html('','Hamburger Tafel') }}</a>
    <a href="https://www.kulturleben-hamburg.de" target="_blank">{{ page.media['UnserAngebot_KulturLebenHamburg_450x300px.jpg'].html('','KulturLeben Hamburg') }}</a>
    <a href="https://foodsharing.de" target="_blank">{{ page.media['UnserAngebot_foodsharing_450x300px.jpg'].html('','foodsharing') }}</a>
    </span>
</div>
