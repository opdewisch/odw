---
title: 'Über uns'
metadata:
    description: 'Op de Wisch ist ein gemeinnütziger Verein und ein Träger der Eingliederungshilfe ASP sowie sozialpädagogischer Familienhilfe. '
    keywords: 'Gemeinnütziger Verein, sozialer Träger, Genesungsbegleiter, Ambulante Sozialpsychiatrie, Eingliederungshilfe, Angehörigenbegleitung, multiprofessionell, Selbsthilfe, multikulturell, Hamburg, HH, Familienhilfe, tiergestützte Pädagogik, Reitprojekt, Unterstützung'
headerImageAlt: 'Team Op de Wisch'
pageColor: green
headerImage:
    user/pages/02.ueber-uns/UeberUns_Header_1920x560px.jpg:
        name: UeberUns_Header_1920x560px.jpg
        type: image/jpeg
        size: 1430077
        path: user/pages/02.ueber-uns/UeberUns_Header_1920x560px.jpg
navigationIcon:
    user/pages/02.ueber-uns/OdW_Icon_UeberUns_170x125px_AKTIV.svg:
        name: OdW_Icon_UeberUns_170x125px_AKTIV.svg
        type: image/svg+xml
        size: 3721
        path: user/pages/02.ueber-uns/OdW_Icon_UeberUns_170x125px_AKTIV.svg
---

<div class="box">
<h2>„Es ist normal, verschieden zu sein.“ (Klaus Dörner)</h2>
<p>Op de Wisch wurde 1990 als gemeinnütziger Verein gegründet und blickt inzwischen auf über 25 Jahre Erfahrung zurück. Unser Name bedeutet „Auf der Wiese“ und ist Sinnbild für die bunte Vielfalt des Lebens, die wir bei uns und unseren KlientInnen wiederfinden. Das ist uns willkommen! Unsere MitarbeiterInnen sind sehr unterschiedlich hinsichtlich Ausbildung und Herkunft. Unsere Teams sind multiprofessionell und vielseitig zusammengesetzt und bestehen aus pädagogisch und psychologisch ausgebildeten MitarbeiterInnen. Mit im Boot sind geschulte GenesungsbegleiterInnen und eine Angehörigenbegleiterin, die wichtige Erfahrungswerte beisteuern. Zusätzlich erleichtern viele verschiedene Sprachen aus dem europäischen, arabischen und afrikanischen Raum das gegenseitige Verstehen.</p>
</div>
<hr>
<div class="grid box">
	<div class="col-6 left col-sm-12">
    	{{ page.media['UeberUns_Content_1_810x400px.jpg'].html('','Halt und Wärme') }}
        {{ page.media['UeberUns_Content_2_810x400px.jpg'].html('','Petra David und Anke Bamberger sind aktiv und fröhlich') }}
    </div>
    <div class="col-6 right col-sm-12">
    <h2>Wichtig ist uns, Sie in Ihrer ganzen Vielfalt und Einzigartigkeit im Blick zu haben.</h2>
	<p>Wir begegnen Ihnen von Anfang an mit Offenheit und Wertschätzung und mit dem Ziel, Sie auf Ihrem Weg zu begleiten. Wir unterstützen Sie mit aktivierenden Hilfen zur Selbsthilfe, um Ihre Selbstbestimmung, Handlungsfähigkeit und Eigenverantwortung zu stärken.</p>
	<p>Wir haben keine Ausschlusskriterien – es sei denn, wir haben nicht den oder die passende MitarbeiterIn für Sie. Den Blick über den eigenen Tellerrand gewährleistet eine begleitende Supervision und die intensive Zusammenarbeit sowohl untereinander als auch mit anderen Organisationen und Einrichtungen.</p>
    </div>
</div>
<hr>
<div class="grid box">
    <div class="col-6 left col-sm-12">
    <h2>Mit Ihren Mitteln können wir noch mehr bewegen</h2>
	<p>Als mildtätige Körperschaft anerkannt durch das Finanzamt Hamburg stellen wir Ihnen gerne für Sach- und Geldspenden entsprechende Bescheinigungen aus. Wenn Sie uns unterstützen möchten, finden Sie unsere Bankverbindung hier:</p>
    <p><strong>Bank für Sozialwirtschaft,<br>
	IBAN DE81 2512 0510 0007 4544 00,<br>
    BIC BFSWDE33HAN.</strong></p>
<p>Ein netter Nebeneffekt, den wir von Herzen allen SpenderInnen wünschen: Für wohltätige Zwecke zu spenden, steigert nachweislich das eigene Wohlbefinden, wie z.B. in der GEOkompakt, Ausgabe 58 ausführlich beleuchtet. Laut einer Studie lag der Zugewinn an Zufriedenheit von Spendern im Mittelwert in etwa so hoch, als hätte man ihre Einkommen verdoppelt. </p>	
<p>Viele kleinere und größere Stiftungen, Unternehmen und Kooperationspartner unterstützen uns und unsere Arbeit bereits und wir sind für alle Geld- und Sachspenden sehr dankbar! Jede Spende kann direkt umgesetzt werden in eine wichtige Leistung für unsere KlientInnen. Wir können aufgrund der Anzahl nicht alle Spender angeben, möchten aber an dieser Stelle stellvertretend einige nennen, wie das Hamburger Spendenparlament, Aktion Mensch, Radio Hamburg, Hamburger Abendblatt, Otto, Shell, die Pestalozzi-Stiftung, die Paul-und-Helmi Nitsch-Stiftung, die Alwine-Fick-Stiftung, Ein Herz für Kinder, die Moser-Stiftung, Hamburgische Brücke, DATEV… Herzlichen Dank. 
</p>
    </div>
    <div class="col-6 right col-sm-12">
    	{{ page.media['UeberUns_Content_3_810x400px.jpg'].html('','Reitprojekt') }}
        {{ page.media['UeberUns_Content_4_810x400px.jpg'].html('','Op de Wisch unterstützt die Hamburger Tafel') }}
        {{ page.media['UeberUns_Content_5_810x400px.jpg'].html('','Spender Op de Wisch') }}
    </div>
</div>
<hr>
<div class="box">
    <h2>Mitbestimmung ist willkommen</h2>
    <p>Einmal im Jahr werden Klientensprecher gewählt, die alle Interessen der Klienten vertreten und im Verein stimmberechtigt sind. Sie engagieren sich für die Weiterentwicklung von Op de Wisch im Sinne der KlientInnen in enger Abstimmung mit der Leitung. Die Klientenvollversammlung bietet zudem eine gute Gelegenheit, sich direkt mit anderen KlientInnen auszutauschen. Generell können bei den Klientensprechern alle Ideen, Wünsche, Kritik oder Lob hinterlegt werden.</p>
</div>
<hr>
<div class="box">
    <h2>Mitgliedschaften und Kooperationen</h2>
	<p>Op de Wisch ist Mitglied im Deutschen Paritätischen Wohlfahrtsverband DPWV, im Dachverband Gemeindepsychiatrie und gehört der AG-Reha an. Wir kooperieren mit SeelenNot und zusätzlich in den Stadtteilen mit verschiedenen anderen Trägern, bei denen Sie auch Angebote wahrnehmen können.</p>
    <span class="logos">
    <a href="http://www.paritaet-hamburg.de/" target="_blank">{{ page.media['UeberUns_Link_DerParitaetische.jpg'].html('','Der Paritätische Hamburg') }}</a>
    <a href="https://www.dvgp.org/" target="_blank">{{ page.media['UeberUns_Link_DG.jpg'].html('','Dachverband Gemeindepsychiatrie e.V.') }}</a>
    <a href="http://www.agreha.de/" target="_blank">{{ page.media['AGreha.png'].html('','AG Reha') }}</a>
    <a href="http://www.seelennot-ev.de/cms/website.php" target="_blank">{{ page.media['SeelennotEV.png'].html('','Seelennot Ev') }}</a>
    <a href="https://www.dgsp-ev.de/" target="_blank">{{ page.media['dgsp-logo.svg'].html('','Deutsche Gesellschaft für Soziale Psychiatrie') }}</a>
    </span>
</div>
