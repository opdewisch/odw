---
title: Startseite
metadata:
    description: 'Das ist die Startseite von Op de Wisch e.V. Wir sind ein sozialer Träger der Eingliederungshilfe mit dem Schwerpunkt ambulante Sozialpsychiatrie ASP sowie der Kinder-, Jugend- und Familienhilfe.'
    keywords: 'Ambulante Sozialpsychiatrie, sozialer Traeger, sozialpädagogische Familienhilfe, Hamburg, Kinder, Familien, Eingliederungshilfe, Reiten, Familienhilfe, Erziehungsbeistandschaft, Unterstützung, Angehörigenbegleitung, Genesungsbegleiter, ambulant, tiergestützte Pädagogik, Hilfe, ASP, psychisch, Psyche, seelisch behindert, seelisch, Gruppenangebote, offene Gruppen, Beratung, Begleitung, Begegnung, Begegnungsstätte, Spenden, Reitprojekt, Pferde, Hunde, heilpädagogisch, sozialintegrativ, Trauma, Feldenkrais, psychische Beratung, Sozialberatung, Angebotsberatung, Therapiepferd, Naturprojekt, interkulturell, Migrationsberatung, Migration, Dari, Farsi, Arabisch, Twi, Migrationshintergrund, psychische Belastung, kostenlose Beratung, HH, Beistand in Krisen, Zuhören, Selbsthilfe, anonym, Wohnassistenz, multikulturell, gemeinsam, Fundraising, Hühner, psycho-soziale Projekte, Gesundheit, Sachspenden, Geldspenden, Erfahrung, diskret, Vertrauen, psychische Hilfe'
footerPage: false
wrapperBackground: true
pageColor: green
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _teaser
            - _intro
            - _slider
            - _news_teaser
---

{% for module in page.collection() %}
{% if module.slug == "_slider" %}
<div class="banner">
  <div class="wrapper">
    <a href="/standorte">
    	<span class="map-link">zu den Standorten</span>
    {{ page.media["OdW_Start_HamburgKarte.svg"].html('Op De Wisch Standorte Karte', 'Op De Wisch Standorte Karte', 'map') }}
    </a>
{% endif %}
{{ module.content }}
{% if module.slug == "_slider" %}
  </div>
</div>
{% endif %}
{% endfor %}
