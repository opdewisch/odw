---
title: Intro
---

# Gemeinsam bewegen wir mehr!
Op de Wisch e.V. ist ein Träger der Eingliederungshilfe mit dem Schwerpunkt Ambulante Sozialpsychiatrie sowie der Kinder-, Jugend- und Familienhilfe. Seit mehr als 25 Jahren begleiten wir Menschen mit Unterstützungsbedarf.

Wir bieten ein vielseitiges ambulantes Hilfsangebot, das immer zum Ziel hat, Ihre jeweilige Lebenssituation zu stabilisieren, Ihre Selbstbestimmung zu stärken und Ihre Genesung zu fördern. An der Planung und Gestaltung der Hilfemaßnahmen sind Sie aktiv beteiligt. Dabei stehen Ihre persönlichen Interessen, Ziele und Möglichkeiten im Mittelpunkt der Aufmerksamkeit.

In unseren fünf Begegnungsstätten treffen Sie auf multikulturelle Teams aus Fachkräften und ausgebildeten Experten aus Erfahrung. Unsere Angebote in verschiedenen Muttersprachen umfassen Beratung, Begleitung und Begegnung.
<br>
