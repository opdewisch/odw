---
title: Teaser
itemsPerRow: 3
mobileItemsPerRow: 1
---

<div class="teaserbox">
  <a class="purple" href="/unser-angebot/ambulante-sozialpsychiatrie">
    <div class="teaserbox-hl">Ambulante Sozialpsychiatrie</div>
    <img class="js-responsive-img" alt="Helfende Hände" data-lsrc="{{ page.media['Start_ASP_650x650px.jpg'].url }}" data-ssrc="{{ page.media['Start_ASP_650x650px.jpg'].crop(0,100,650,360).url }}">
  </a>
</div>

---

<div class="teaserbox">
  <a class="lightgreen" href="/projekte">
    <div class="teaserbox-hl">Projekte</div>
    <img class="js-responsive-img" alt="Huhn probiert etwas Neues aus" data-lsrc="{{ page.media['Start_Projekte_650x650px.jpg'].url }}" data-ssrc="{{ page.media['Start_Projekte_650x650px.jpg'].crop(0,100,650,360).url }}">
  </a>
</div>

---

<div class="teaserbox">
  <a class="purple" href="/unser-angebot/kinder-jugend-und-familienhilfe">
    <div class="teaserbox-hl">Kinder-, Jugend- & Familienhilfe</div>
    <img class="js-responsive-img" alt="Hilfe für Kinder und Jugendliche" data-lsrc="{{ page.media['Start_KiJuFaHilfe_650x650px.jpg'].url }}" data-ssrc="{{ page.media['Start_KiJuFaHilfe_650x650px.jpg'].crop(0,100,650,360).url }}">
  </a>
</div>
