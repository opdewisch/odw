---
title: 'News Teaser'
itemsPerRow: 2
mobileItemsPerRow: 1
---

<div class="teaserbox">
  <a class="blue" href="/programm">
    <div class="teaserbox-hl">Programm</div>
    {{ page.media['Start_Programm_870x280px.jpg'].html }}
  </a>
</div>

---

<div class="teaserbox">
<a class="yellow" href="/aktuelles">
  <div class="teaserbox-hl">Aktuelles</div>
  {% for news in page.find('/aktuelles').children.order('header.news_date', 'desc', null, 1).slice(0, 3) %}
  <div class="recent-news">
    <span class="recent-news-title">{{ news.title }}</span>
  </div>
  {% endfor %}
</a>
</div>
