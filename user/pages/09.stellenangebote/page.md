---
title: Stellenangebote
expires: 'no cache'
metadata:
    description: 'Hier findet man die Beschreibung von Op de Wisch als Arbeitgeber und alle aktuellen Stellenangebote. '
    keywords: 'Bewerbung, Bundesfreiwilligendienst, Sozialpädagoge, Psychologe, Praktikum, Stellenangebot, Familienhilfe, Hamburg, HH, Bufdi, Praktikum'
footerPage: true
headerImageAlt: 'Op de Wisch sucht neue Tassen im Schrank'
pageColor: grey
headerImage:
    user/pages/09.stellenangebote/Stellenangebote_Header_1920x560px.jpg:
        name: Stellenangebote_Header_1920x560px.jpg
        type: image/jpeg
        size: 518794
        path: user/pages/09.stellenangebote/Stellenangebote_Header_1920x560px.jpg
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
---

<div class="grid box">
    <div class="col-6 left col-sm-12">
    {{ page.media['Stellenangebote_Content_1_810x400px.jpg'].html('','Helfende Haende') }}
    </div>
    <div class="col-6 right col-sm-12">
    	<h2>Op de Wisch als Arbeitgeber</h2>
        <p><strong>Leistungsstark</strong><br>
    Op de Wisch bezahlt Sie nach dem Tarif Pari HH und leistet einen Beitrag zur betrieblichen Altersversorgung.</p>
        <p><strong>Qualifiziert</strong><br>
    Unsere MitarbeiterInnen prägen die inhaltliche Qualität unseres Hilfsangebotes. Das unterstützt Op de Wisch, indem wir Fort- und Weiterbildungen fördern. Auch die Mitarbeiterzufriedenheit wird regelmäßig ermittelt.</p>
        <p><strong>Inspiriert</strong><br>
    Wer sich selbstbestimmt und kreativ entfalten will, braucht gestaltbare Spielräume. Die finden Sie bei uns und dazu KollegInnen, mit denen Sie ideenreich zusammenarbeiten können.</p>
        <p><strong>Mobil</strong><br>
    Ohne Fahrerei geht in Hamburg nichts: Op de Wisch beteiligt sich daher mit Vergünstigungen an der Proficard des HVV.</p>
    </div>
</div>
<hr>
<div class="grid box">
<div class="col-6 left col-sm-12">
    <h2>Aktuelle Stellenangebote</h2>
	<p>Sie sind mit uns auf einer Linie? Dann freuen wir uns, wenn Sie sich auf aktuelle Ausschreibungen bewerben, die hier als PDF hinterlegt sind.</p>
  {{ page.children|first.content }}
  </div>
  <div class="col-6 right col-sm-12">
  	{{ page.media['Stellenangebote_Content_2_810x400px.jpg'].html('','Arbeiten bei Op De Wisch') }}
  </div>
</div>
<hr>
<div class="box">
    <h2>Op de Wisch als Arbeitgeber</h2>
	<p>Wenn wir zur Zeit keine passende Stelle für Sie haben, schicken Sie uns gerne Ihre Initiativbewerbung an <strong>bewerbung@op-de-wisch.de</strong>.</p>
    <p>Haben Sie generell Fragen bezüglich Ihrer Möglichkeiten bei Op de Wisch, rufen Sie am besten gleich durch.</p>
	<p>Wir sind anerkannt als Einsatzstelle im Bundesfreiwilligendienst und freuen uns ganzjährig über Bewerbungen für diese Position sowie für andere Praktika. Wir interessieren uns für Sie, unabhängig von Alter, Geschlecht, Religion, Herkunft oder anderen Merkmalen. Aufgrund unseres Arbeitsfeldes ist eine gewisse Lebenserfahrung und Erfahrung im Umgang mit psychischen Erkrankungen willkommen.</p>
</div>
