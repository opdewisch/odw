---
title: 'Aktuelle Stellenangebote'
joboffers:
    -
        file:
            'user/pages/09.stellenangebote/_aktuelle-stellenangebote/Mitarbeiter KiJu Farsi-Dari.pdf':
                name: 'Mitarbeiter KiJu Farsi-Dari.pdf'
                type: application/pdf
                size: 104825
                path: 'user/pages/09.stellenangebote/_aktuelle-stellenangebote/Mitarbeiter KiJu Farsi-Dari.pdf'
    -
        file:
            'user/pages/09.stellenangebote/_aktuelle-stellenangebote/SozPäd KiJu Ausschreibung.pdf':
                name: 'SozPäd KiJu Ausschreibung.pdf'
                type: application/pdf
                size: 85294
                path: 'user/pages/09.stellenangebote/_aktuelle-stellenangebote/SozPäd KiJu Ausschreibung.pdf'
    -
        file:
            'user/pages/09.stellenangebote/_aktuelle-stellenangebote/BFD Ausschreibung.pdf':
                name: 'BFD Ausschreibung.pdf'
                type: application/pdf
                size: 287760
                path: 'user/pages/09.stellenangebote/_aktuelle-stellenangebote/BFD Ausschreibung.pdf'
---

