<?php
namespace Grav\Plugin;
class CSVTwigExtension extends \Twig_Extension
{
    private $ignored_lines = 0;
    public function __construct($ignored_lines) {
      $this->ignored_lines = $ignored_lines;
    }
    public function getName()
    {
        return 'CSVTwigExtension';
    }
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('csv', [$this, 'csvParser'])
        ];
    }
    public function openCsvFile($fileName)
    {
      if(($handle = @file_get_contents($fileName)) !== FALSE) {
        return self::parseCsvData($handle, self::detectDelimiter($fileName));
      }
      return "<p style='color: red;'>Cannot read file: " . __DIR__ . $fileName ."</p>";
    }
    public function detectDelimiter($fileName)
    {
        $delimiters = array(
            ';' => 0,
            ',' => 0,
            "\t" => 0,
            "|" => 0
        );

        $handle = fopen($fileName, "r");
        $firstLine = fgets($handle);
        fclose($handle);
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($firstLine, $delimiter));
        }

        return array_search(max($delimiters), $delimiters);
    }
    private function groupBy($arr, $index=0) {
      $result = array();

      foreach ($arr as $data) {
        if(!is_null($data[0])) {
          $id = $data[$index];
          if (isset($result[$id])) {
             $result[$id][] = $data;
          } else {
             $result[$id] = array($data);
          }
        }
      }
      return $result;
    }
    private function parseCsvData($data, $delimiter)
    {
      if($lines = explode(PHP_EOL, $data)) {
        $rows = array();
        foreach ($lines as $line) {
          $rows[] = str_getcsv($line, $delimiter);
        }
        return $rows;
      }
      return "<p style='color: red;'>Cannot parse CSV file</p>";
    }
    private function cleanArray($array, $count) {
      if($count > 0) {
        return array_splice($array, $count);
      }
      return $array;
    }
    public function csvParser($fileName)
    {
      $rows = self::openCsvFile($fileName);
      $sorted_rows = self::groupBy($rows, 4);
      $cleaned_sorted_rows = self::cleanArray($sorted_rows, $this->ignored_lines);
      return $cleaned_sorted_rows;
    }
}
