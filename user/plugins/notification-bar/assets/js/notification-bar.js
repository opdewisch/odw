var notifications = document.querySelectorAll('.notifications-item');

for(var i = 0; notifications.length > i; i++) {
  if(notifications[i].getAttribute('data-delay')) {
    (function(ind) {
      setTimeout(function(){
        notifications[ind].classList.add('closed');
      }, notifications[ind].getAttribute('data-delay'));
    })(i);
  }
}

window.closeNotification = function(id) {
  document.querySelector('.notifications-item[data-id="'+ id +'"]').classList.add('closed');
}
