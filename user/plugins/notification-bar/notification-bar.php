<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use RocketTheme\Toolbox\Event\Event;

/**
 * Class NotificationBarPlugin
 * @package Grav\Plugin
 */
class NotificationBarPlugin extends Plugin
{
  /**
   * @var Notifications
   */
   private $notifications = [];

  /**
   * @return array
   *
   * The getSubscribedEvents() gives the core a list of events
   *     that the plugin wants to listen to. The key of each
   *     array section is the event that the plugin listens to
   *     and the value (in the form of an array) contains the
   *     callable (or function) as well as the priority. The
   *     higher the number the higher the priority.
   */
  public static function getSubscribedEvents()
  {
    return [
      'onTwigSiteVariables'   => ['onTwigSiteVariables', 0],
      'onPageInitialized' => ['onPageInitialized', 0],
      'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
      'onAssetsInitialized'   => ['onAssetsInitialized', 0]
    ];
  }

  /**
   * Initialize the plugin
   */
  public function onPageInitialized()
  {
    // Don't proceed if we are in the admin plugin
    if ($this->isAdmin()) {
      return;
    }

    $page = $this->grav['page'];
    $uri = $this->grav['uri'];

    $notifications = $this->config->get('plugins.notification-bar.notifications');

    foreach ($notifications as &$notification) {
      $pages = $this->config->get('plugins.notification-bar.pages');

      if(is_array($notification['pages']) && in_array($page->rawRoute(), $notification['pages'])) {

            $parsedown = new \Parsedown();
            $text = $parsedown->text($notification['text']);


        $this->notifications[] = [
          'content' => $text,
          'time' => $notification['time'],
          'color' => $notification['color'],
          'id' => $notification['id'],
          'classes' => $notification['classes'],
        ];
      }
    }
  }


  /**
   * Add twig paths to plugin templates.
   */
  public function onTwigTemplatePaths()
  {
      $twig = $this->grav['twig'];
      $twig->twig_paths[] = __DIR__ . '/templates';
  }

  /**
   * Initialize the plugin
   */
  public function onTwigSiteVariables()
  {
    $twig = $this->grav['twig'];

    // Get a variable from the plugin configuration
    $twig->twig_vars['notificationBar_notifications'] = $this->notifications;
  }

  public function onAssetsInitialized()
  {
    $this->grav['assets']->add('plugin://notification-bar/assets/css/notification-bar.min.css');
    $this->grav['assets']->add('plugin://notification-bar/assets/js/notification-bar.js', 1);
  }
}
