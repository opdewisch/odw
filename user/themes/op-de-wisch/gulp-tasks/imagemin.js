var plugins = require('gulp-load-plugins')();
var gulp = require('gulp');
var folders = require('./settings.js');

gulp.task('imagemin', function() {
  return gulp.src('images/**/*')
    .pipe(plugins.imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{
        cleanupIDs: false
      }]
    }))
    .pipe(gulp.dest('images'));
});