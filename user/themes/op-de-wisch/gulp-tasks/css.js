var plugins = require('gulp-load-plugins')();
var gulp = require('gulp');
var merge = require('merge-stream');
var folders = require('./settings.js');

gulp.task('sass', function () {
  var global = gulp.src(folders.source + '/scss/importer.scss')
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass().on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer())
    .pipe(plugins.rename('index.css'))
    .pipe(plugins.sourcemaps.write('/'))
    .pipe(gulp.dest(folders.dist + '/css'));
  var programm = gulp.src(folders.source + '/scss/print/programm.scss')
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass().on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer())
    .pipe(plugins.rename('programm--print.css'))
    .pipe(plugins.sourcemaps.write('/'))
    .pipe(gulp.dest(folders.dist + '/css'));

  return merge(global, programm);
});

gulp.task('css-minify', ['sass'], function () {
  var global = gulp.src(folders.dist + '/css/index.css')
    .pipe(plugins.cssmin({keepSpecialComments: 0}))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest(folders.dist + '/css'));
  var programm = gulp.src(folders.dist + '/css/programm--print.css')
    .pipe(plugins.cssmin({keepSpecialComments: 0}))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest(folders.dist + '/css'));

  return merge(global, programm);
});
