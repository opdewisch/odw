const $ = require('jquery');
const GdprCookieConsent = {
    cookiesAllowed: false,
    cookieConsentEl: $('body').find(`[data-id='gdpr-cookie-consent']`),
    getCookie: function(name) {
      return document.cookie.indexOf(name) >= 0;
    },
    init: function() {
      this.cookiesAllowed = this.getCookie('cookie-agreed');
      if(this.cookieConsentEl.length && !this.cookiesAllowed) {
        this.cookieConsentEl.show();
      }
    },
    cookieAgreed: function() {
      var days = 365;
      var date = new Date();
      var expires = null;
      date.setTime(date.getTime() + (days*24*60*60*1000));
      expires = "; expires=" + date.toUTCString();
      document.cookie = "cookie-agreed=1"  + expires + "; path=/";
    },
};

module.exports = GdprCookieConsent;
