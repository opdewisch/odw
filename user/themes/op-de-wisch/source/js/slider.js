const $ = require('jquery');
const Wallop = require('wallop');
const Slider = {
    sliderInterval: '',
    sliderEl: $('.slider')[0],
    slider: '',
    init: function() {
      this.slider = new Wallop(this.sliderEl);
      this.startSlide();
      this.slider.on('change', this.onSlide);

      var that = this;
      $(".Wallop-dot").click(function() {
        that.slider.goTo($(this).data("index"));
        clearInterval(that.sliderInterval);
        that.startSlide();
      })
    },
    startSlide: function() {
      var that = this;
      that.sliderInterval = setInterval(function() {
        that.slider.next();
      }, 5000);
    },
    onSlide: function(event) {
      const dots = $(".Wallop-dot");
      $(".Wallop-dot--current").removeClass("Wallop-dot--current");
      dots.eq(event.detail.currentItemIndex).addClass("Wallop-dot--current");
    }
};

module.exports = Slider;
