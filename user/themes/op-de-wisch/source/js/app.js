const $ = require('jquery');
const Navigation = require('./navigation');
const Slider = require('./slider');
const ResponsiveImages = require('./responsiveImages');
const Hyphenator = require('./Hyphenator');
const GdprCookieConsent = require('./gdprCookieConsent');

const SMALL = 'small';
const MEDIUM = 'medium';
const LARGE = 'large';

window.App = {};

App = {
    breakpoints: {
        medium: 699,
        large: 1200
    },
    isTouchDevice: 'ontouchstart' in document.documentElement,
    menu: Navigation,

    init: function() {
        this.checkTouchDevices();
        this.setBreakpoint();
        $(window).resize(this.setBreakpoint.bind(this));
        Navigation.init();
        GdprCookieConsent.init();
        if($('.slider').length > 0) {
          Slider.init();
        }
        ResponsiveImages.init();
        window.Hyphenator = Hyphenator;
        Hyphenator.config({
        	defaultlanguage: 'de'
        });
        Hyphenator.run();
        window.cookieAgreed = GdprCookieConsent.cookieAgreed;
    },

    checkTouchDevices: function() {
        if(this.isTouchDevice) {
            $('body').addClass('touch');
        } else {
            $('body').addClass('no-touch');
        }
    },

    setBreakpoint: function() {
        const width = $(window).width();
        const previousBreakpoint = this.currentBreakpoint;

        if(width < this.breakpoints.medium) {
            this.currentBreakpoint = SMALL;
        } else if(width < this.breakpoints.large) {
            this.currentBreakpoint = MEDIUM;
        } else {
            this.currentBreakpoint = LARGE;
        }

        if(this.currentBreakpoint != previousBreakpoint) {
            $(App).trigger('breakpointChanged', App.currentBreakpoint);
            App.update();
        }
    },
    update: function() {

    }
};

$(() => {
    App.init();
});
