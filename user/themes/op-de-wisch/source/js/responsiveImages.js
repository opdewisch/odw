const $ = require('jquery');
const ResponsiveImages = {
    images: $('.js-responsive-img'),
    shortBreakpoint: 'l',
    init: function() {
      this.shortBreakpoint = App.currentBreakpoint.charAt(0);
      $(App).on('breakpointChanged', (newBreakpoint) => {
        this.shortBreakpoint = newBreakpoint;
        this.loadImages();
      });
      this.loadImages();
    },
    loadImages: function() {
      this.images.each((index, el) => {
        this.changeSrc($(el));
      });
    },
    changeSrc: function(img) {
      if(this.hasSrc(img, this.shortBreakpoint)) {
        img.attr('src', img.data(this.shortBreakpoint + "src"));
      } else if(this.hasSrc(img, 'l')) {
        img.attr('src', img.data("lsrc"));
      } else {
        throw 'One ResponsiveImage has not a valid markup';
      }
    },
    hasSrc: function(img, breakpoint) {
      return img.data(breakpoint +"src") !== undefined;
    }
};

module.exports = ResponsiveImages;
