const $ = require('jquery');
const Navigation = {
  header: $('.header'),
  mainMenuButton: $('#header-nav-toggle'),
  mainMenu: $('.header-nav'),
  mainMenuList: $('.header-nav-list'),
  selectedMenuItem: $('.header-nav-list li.selected'),
  menuOpenCounter: 0,
  notifications: $('.notifications-item'),

  init: function() {
    if(App.currentBreakpoint === 'small') {
      this.mainMenuButton.on('touchstart', (event) => { this.handleMenuClick(event); });
      this.mainMenuList.find('.js-navigation--trigger').on('touchstart', (event) => { this.expandSubmenu(event); });
      $(window).on('resize.mobile', () => { this.justifyFirstPaint(); });
      this.notifications.each((i,el) => {
        const that = this;
        const animationDuration = parseFloat($(el).css('transition-duration').replace(/[^-\d\.]/g, '')) * 1000;
        setTimeout(() => { $('body').animate({paddingTop: '-=' + $(el).outerHeight() + 'px'}, animationDuration); }, $(el).data('delay'));
      });

      this.justifyFirstPaint();

      $(App).on('breakpointChanged', (newBreakpoint) => {
        if(newBreakpoint !== 'small') {
          $(window).off('resize.mobile');
          setTimeout(() => { $('body').css('padding-top', '0px'); },10);
        }
      })
    }
  },
  justifyFirstPaint: function() {
    const headerHeight = this.header.outerHeight();
    console.log(headerHeight);
    $('body').css('padding-top', headerHeight + 'px');
  },
  handleMenuClick: function(event) {
    event.preventDefault();
    this.mainMenu.toggleClass('open');
    if(this.menuOpenCounter == 0 && this.selectedMenuItem.find('svg').length > 0) {
        this.animateSelectedMenuItem();
    }
    this.menuOpenCounter++;
    $('body').toggleClass('menu-is-open');
  },
  expandSubmenu: function(event) {
    $(event.currentTarget).parent().find('ul').toggleClass('open')
    event.preventDefault();
  },
  animateSelectedMenuItem: function() {
    const $svgEl = this.selectedMenuItem.find('svg');
    const $textEl = this.selectedMenuItem.find('.header-nav-list-item');
    const rightOffset = $textEl.width();
    const padding = 4;
    const $lineEl = this.selectedMenuItem.find('.line');
    $lineEl.css('left', $svgEl.offset().left + 'px');

    $lineEl.animate({
      'left': rightOffset + padding + 'px'
    });
  },
  close: function(event) {
    this.mainMenu.removeClass('open');
    $('body').removeClass('menu-is-open');
  }
};

module.exports = Navigation;
